package com.psybergate.grade2021.core.jeebasicsinterface.ex1.client.vendor;

import com.psybergate.grade2021.core.jeebasicsinterface.ex1.client.standard.Date;
import com.psybergate.grade2021.core.jeebasicsinterface.ex1.client.standard.DateFactory;
import com.psybergate.grade2021.core.jeebasicsinterface.ex1.client.standard.InvalidDateException;

public class DataFactoryImpl implements DateFactory {
  @Override
  public Date createDate(int day, int month, int year) throws InvalidDateException {
    return new DateImpl(day,month,year);
  }
}

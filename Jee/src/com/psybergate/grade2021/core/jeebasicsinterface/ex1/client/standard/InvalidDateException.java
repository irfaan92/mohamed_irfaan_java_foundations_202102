package com.psybergate.grade2021.core.jeebasicsinterface.ex1.client.standard;

public class InvalidDateException extends Exception{

  public InvalidDateException(String message) {
    super(message);
  }
}

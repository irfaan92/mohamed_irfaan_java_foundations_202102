package com.psybergate.grade2021.core.jeebasicsinterface.ex1.client.standard;

public interface DateFactory {
  Date createDate(int day,int month,int year) throws InvalidDateException;
}

package com.psybergate.grade2021.core.jeebasicsinterface.ex1.client.standard;

public interface Date {
  int getDay();

  int getMonth();

  int getYear();

  boolean isLeapYear();

  Date addDays(int noOfDays) throws InvalidDateException;
}

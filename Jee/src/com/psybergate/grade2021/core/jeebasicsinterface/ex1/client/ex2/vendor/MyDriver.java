package com.psybergate.grade2021.core.jeebasicsinterface.ex1.client.ex2.vendor;

import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Logger;

public class MyDriver implements Driver {
  static {
    try{
      MyDriver myDriver = new MyDriver();
      DriverManager.registerDriver(myDriver);
      System.setSecurityManager(new RMISecurityManager());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public Connection connect(String url, Properties info) throws SQLException {
    return new MyConnection();
  }

  @Override
  public boolean acceptsURL(String url) throws SQLException {
    return false;
  }

  @Override
  public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
    return new DriverPropertyInfo[0];
  }

  @Override
  public int getMajorVersion() {
    return 0;
  }

  @Override
  public int getMinorVersion() {
    return 0;
  }

  @Override
  public boolean jdbcCompliant() {
    return false;
  }

  @Override
  public Logger getParentLogger() throws SQLFeatureNotSupportedException {
    return null;
  }
}

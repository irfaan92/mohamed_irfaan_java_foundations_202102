package com.psybergate.grade2021.core.jeebasicsinterface.ex1.client.vendor;

import com.psybergate.grade2021.core.jeebasicsinterface.ex1.client.standard.Date;
import com.psybergate.grade2021.core.jeebasicsinterface.ex1.client.standard.InvalidDateException;

import java.time.LocalDate;

public class DateImpl implements Date {
  public static final int[] DAYS_IN_MONTH = {31,28,31,30,31,30,31,31,30,31,30,31};
  public static final int DAYS_IN_FEB_LEAP_YEAR = 29;
  private int day;
  private int month;
  private int year;

  public DateImpl(int day, int month, int year) throws InvalidDateException {
    this.day = day;
    this.month = month;
    this.year = year;
    checkDate();
  }

  public void checkDate() throws InvalidDateException {
    checkDay();
    checkMonth();
  }

  private void checkMonth() throws InvalidDateException {
    if(month<1 || month >12){
      throw new InvalidDateException("Invalid month");
    }
  }

  private void checkDay() throws InvalidDateException {
    if(day>DAYS_IN_MONTH[month-1] && !isLeapYear()){
      throw new InvalidDateException("Invalid day");
    }
    else if (day > DAYS_IN_FEB_LEAP_YEAR && isLeapYear() && month==2) {
      throw new InvalidDateException("Invalid day");
    }
  }

  @Override
  public int getDay() {
    return day;
  }

  @Override
  public int getMonth() {
    return month;
  }

  @Override
  public int getYear() {
    return year;
  }

  @Override
  public boolean isLeapYear() {
    if (year % 4 == 0) {
      if (year % 100 == 0) {
        if (year % 400 == 0) {
          return true;
        }
        else {
          return false;
        }
      }
      else{
        return true;
      }
    }
    return false;
  }

  @Override
  public Date addDays(int noOfDays) throws InvalidDateException {
    LocalDate date = LocalDate.now().plusDays(noOfDays);
    return new DateImpl(date.getDayOfMonth(), date.getMonthValue(),date.getYear());
  }
}

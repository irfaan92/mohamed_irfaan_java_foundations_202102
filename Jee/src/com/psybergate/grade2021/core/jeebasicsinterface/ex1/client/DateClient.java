package com.psybergate.grade2021.core.jeebasicsinterface.ex1.client;

import com.psybergate.grade2021.core.jeebasicsinterface.ex1.client.standard.Date;
import com.psybergate.grade2021.core.jeebasicsinterface.ex1.client.standard.DateFactory;
import com.psybergate.grade2021.core.jeebasicsinterface.ex1.client.standard.DateFactoryLoader;
import com.psybergate.grade2021.core.jeebasicsinterface.ex1.client.standard.InvalidDateException;

public class DateClient {
  public static void useDates() throws InvalidDateException {
    DateFactory dateFactory = DateFactoryLoader.getDateFactory();
    Date d=dateFactory.createDate(10,10,2001);
    System.out.println(d.getMonth());
    System.out.println("dateFactory.getClass() = " + dateFactory.getClass());
    System.out.println("d.getClass() = " + d.getClass());

  }

  public static void main(String[] args) throws InvalidDateException {
    useDates();
  }
}

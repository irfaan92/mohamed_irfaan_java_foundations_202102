package com.psybergate.grade2021.core.jeebasicsinterface.ex1.client.standard;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DateFactoryLoader {
  public static DateFactory dateFactory;
  public static DateFactory getDateFactory(){
    if (dateFactory != null) {
      return dateFactory;
    }
    try(InputStream is = DateFactory.class.getClassLoader().getResourceAsStream("date.properties")) {
      System.out.println(is);
      Properties properties = new Properties();
      properties.load(is);
      String dateFactoryClass = properties.getProperty("date.factory");
      dateFactory = (DateFactory) Class.forName(dateFactoryClass).newInstance();
      return dateFactory;
    } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
      throw new RuntimeException("error loading class",e);
    }
  }
}

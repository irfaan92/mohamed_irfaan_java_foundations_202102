package com.psybergate.grade2021.core.collections.homework1;

import java.util.*;

public class Customer implements Comparable{

  private final customerComparator customerComparator =
      new customerComparator();

  public static void main(String[] args) {
    Customer customer1 = new Customer(1, "Irfaan", "Lenasia");
    Customer customer2 = new Customer(2, "Chris", "Lenasia");
    Customer customer3 = new Customer(3, "Kubaib", "Lenasia");
    Customer customer4 = new Customer(1, "Irfaan", "Lenasia");
    SortedSet set = new TreeSet();
    set.add(customer1);
    set.add(customer2);
    set.add(customer3);
    set.add(customer4);
    for (Object o : set) {
      System.out.println(o);
      System.out.println(o.hashCode());
    }
    SortedSet setReverse= ((TreeSet) set).descendingSet();
    for (Object o : setReverse) {
      System.out.println(o);
    }
  }

  private int customerNum;
  private String customerName;
  private String address;

  public Customer(int customerNum, String customerName, String address) {
    this.customerNum = customerNum;
    this.customerName = customerName;
    this.address = address;
  }

  @Override
  public String toString() {
    return "Customer{" +
        "customerNum=" + customerNum +
        ", customerName='" + customerName + '\'' +
        ", address='" + address + '\'' +
        '}';
  }
  

  @Override
  public int compareTo(Object o) {
    return this.customerNum-((Customer) o).customerNum;

  }


}

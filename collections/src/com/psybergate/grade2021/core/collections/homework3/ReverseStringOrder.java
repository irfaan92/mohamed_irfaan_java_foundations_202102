package com.psybergate.grade2021.core.collections.homework3;

import java.util.*;

public class ReverseStringOrder {
  public static void main(String[] args) {

    SortedSet words = new TreeSet(Collections.reverseOrder());
    words.add("facebook");
    words.add("suicide");
    words.add("liverpool");
    words.add("love");
    for (Object word : words) {
      System.out.println(word);
    }
  }
}

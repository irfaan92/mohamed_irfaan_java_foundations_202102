package com.psybergate.grade2021.core.collections.homework2;

import java.util.SortedMap;
import java.util.TreeMap;

public class Main {
  public static void main(String[] args) {
    Employee employee1 = new Employee(1, 10);
    Employee employee2 = new Employee(2, 20);
    SortedMap employees = new TreeMap();
    employees.put(employee1,employee1.getEmployeeAnnualSalary());
    employees.put(employee2,employee2.getEmployeeAnnualSalary());
    for (Object o : employees.keySet()) {
      System.out.println(o);
      System.out.println(employees.get(o));
    }

  }
}

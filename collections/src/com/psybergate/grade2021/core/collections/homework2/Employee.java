package com.psybergate.grade2021.core.collections.homework2;

public class Employee implements Comparable{
  private int employeeId;
  private int employeeAnnualSalary;

  public Employee(int employeeId, int employeeAnnualSalary) {
    this.employeeId = employeeId;
    this.employeeAnnualSalary = employeeAnnualSalary;
  }

  public int getEmployeeAnnualSalary() {
    return employeeAnnualSalary;
  }

  @Override
  public String toString() {
    return "Employee{" +
        "employeeId=" + employeeId +
        ", employeeAnnualSalary=" + employeeAnnualSalary +
        '}';
  }



  @Override
  public int compareTo(Object o) {
    return ((Employee)o).employeeId-this.employeeId;
  }
}

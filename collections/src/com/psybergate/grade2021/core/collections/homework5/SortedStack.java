package com.psybergate.grade2021.core.collections.homework5;

import com.psybergate.grade2021.core.collections.exercise1.com.psybergate.mentoring.lang.collections.Array;

public interface SortedStack extends Array {
  boolean comparator(Object obj, int position);
}

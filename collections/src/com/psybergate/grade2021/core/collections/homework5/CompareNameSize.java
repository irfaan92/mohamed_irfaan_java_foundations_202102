package com.psybergate.grade2021.core.collections.homework5;

import java.util.Comparator;

public class CompareNameSize implements Comparator {

  @Override
  public int compare(Object o1, Object o2) {
    return ((String)o1).length()-((String)o2).length();
  }
}

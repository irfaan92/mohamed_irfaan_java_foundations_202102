package com.psybergate.grade2021.core.collections.homework5;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.*;

public class TreeStack implements SortedStack {

  public static void main(String[] args) {
    SortedStack sortedStack = new TreeStack();
    sortedStack.add("hello");
    sortedStack.add("my friend");
   sortedStack.add("snoop");
    sortedStack.add("dre");
    sortedStack.add("mm");
    sortedStack.add("zebra");
  //  Arrays.sort(sortedStack);
    for (Object o : sortedStack) {
      System.out.println(o);
    }
  }

  private Object[] obj = new Object[100];

  private int size;

  private Comparator comparator;

  public TreeStack() {

  }

  public TreeStack(Comparator comparator) {
    this.comparator = comparator;
  }

  @Override
  public void push(Object obj) {
    add(obj);

  }

  @Override
  public Object pop() {
    Object last = obj[--size];
    obj[size] = null;
    return last;
  }

  @Override
  public Object get(int position) {
    return obj[size - position];
  }

  @Override
  public boolean comparator(Object obj, int position) {
    if (this.comparator != null) {
      return this.comparator.compare(this.obj[position], obj) < 0;
    } else {
      return ((Comparable) this.obj[position]).compareTo(obj) < 0;
    }
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    if (size == 0) {
      return true;
    }
    return false;
  }

  @Override
  public boolean contains(Object o) {
    for (Object o1 : obj) {
      if (o.equals(o1)) {
        return true;
      }

    }
    return false;
  }

  public int findIndex(Object obj){
    int indexCounter=0;

      while (indexCounter<size-1&&comparator(obj, indexCounter)) {
        indexCounter++;
      }

    return indexCounter;
  }



  @Override
  public Iterator iterator() {
    Iterator iterator = new Iterator() {
      int index = size-1;

      @Override
      public boolean hasNext() {
        return index > 0;
      }

      @Override
      public Object next() {
        return obj[index--];

      }

    };
    return iterator;
  }

  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public boolean add(Object o) {

    size++;
    int position = findIndex(o);
    placeInArray(position, o);

    return true;

  }

  private void placeInArray(int position,Object obj) {
    Object[] newarr = new Object[size];
    for (int i = 0; i < size; i++) {
      if (i < position) {
        newarr[i] = this.obj[i];
      } else if (i == position) {
        newarr[i] = obj;
      } else {
        newarr[i] = this.obj[i-1];
      }
    }
    this.obj = newarr;

  }

  @Override
  public boolean remove(Object o) {
    throw new NotImplementedException();
  }

  @Override
  public boolean addAll(Collection c) {
    throw new NotImplementedException();
  }

  @Override
  public void clear() {
    throw new NotImplementedException();
  }

  @Override
  public boolean retainAll(Collection c) {
    throw new NotImplementedException();
  }

  @Override
  public boolean removeAll(Collection c) {
    throw new NotImplementedException();
  }

  @Override
  public boolean containsAll(Collection c) {
    throw new NotImplementedException();
  }

  @Override
  public Object[] toArray(Object[] a) {
    throw new NotImplementedException();
  }
}

package com.psybergate.grade2021.core.collections.homework6;

import java.util.LinkedList;
import java.util.List;

public class MyHashSet {
  public static void main(String[] args) {
    MyHashSet myHashSet = new MyHashSet();
    myHashSet.add(1);
    myHashSet.add(0);
    myHashSet.add(3);
    System.out.println("myHashSet.size() = " + myHashSet.size());
    myHashSet.iterator();

  }

  private int bucketSize = 15000;

  private int size;

  private List[] buckets;

  public MyHashSet() {
    buckets = new List[bucketSize];
  }

  public void add(int value) {
    int bucketValue = hash(value);
    if (buckets[bucketValue] == null) {
      buckets[bucketValue] = new LinkedList();
    }
    if (isValueInBucket(value, buckets[bucketValue])) {
      buckets[bucketValue].add(value);
      size++;
    }
  }

  private boolean isValueInBucket(int value, List bucket) {
    return bucket.indexOf(value) == -1;
  }

  public int size() {
    return size;
  }

  public void remove(int value) {
    int bucketValue = hash(value);
    if (buckets[bucketValue] == null) {
      return;
    }
    if (isValueInBucket(value, buckets[bucketValue])) {
      buckets[bucketValue].remove(value);
      size--;
    }
  }

  public int hash(int value) {
    return Integer.hashCode(value) % bucketSize;
  }

  public void iterator() {
    int counter = 0;
    while (counter < bucketSize) {
      if (buckets[counter] == null) {
        counter++;
        continue;
      }
      for (Object bucket : buckets[counter]) {
        System.out.println(bucket);
        counter++;
      }
    }
  }
}

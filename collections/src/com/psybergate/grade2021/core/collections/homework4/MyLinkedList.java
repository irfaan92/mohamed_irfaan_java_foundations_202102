package com.psybergate.grade2021.core.collections.homework4;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyLinkedList extends AbstractMyLinkList {
  private int size = 0;

  class Node {
    Object data;

    Node next;

    public Node(Object data) {
      this.data = data;
      this.next = null;
    }
  }

  public Node head = null;

  public Node tail = null;

  public boolean add(Object data) {

    Node newNode = new Node(data);

    if (head == null) {
      head = newNode;
      tail = newNode;
    } else {
      tail.next = newNode;
      tail = newNode;
    }
    size++;
    return true;
  }

  public void deleteNodeByValue(Object data) {
    Node current = head;
    Node previous = null;
    if (head.data == data) {
      head = head.next;
    } else {
      while (true) {
        if (current == null) {
          break;
        }
        if (current.data == data) {
          break;
        }
        previous = current;
        current = current.next;
      }
      if (current == null) {
        throw new NullPointerException();
      } else {
        previous.next = current.next;
      }
    }
  }

  public Object remove(int value) {
    Node current = head;
    Node previous = null;
    int counter = 1;
    if (value > size) {
      System.out.println("value is beyond bonds");
    } else if (value == 1) {
      head = head.next;
    } else {
      while (true) {
        counter++;
        previous = current;
        current = current.next;
        if (current == null) {
          break;
        }
        if (value == counter) {
          break;
        }
        previous = current;
        current = current.next;
      }
      if (current == null) {
        System.out.println("Element not present in list");
      } else {
        previous.next = current.next;
      }
    }

    return null;
  }

  public void display() {
    Node current = head;

    if (head == null) {
      System.out.println("List is empty");
      return;
    }

    while (current != null) {
      System.out.print(current.data + " ");
      current = current.next;
    }
    System.out.println();
  }

  public Object set(int index, Object obj) {
    Node current = head;
    Node previous = null;
    Node removeNode = null;
    Node insertNode = new Node(obj);
    int counter = 0;
    if (size == 0) {
      throw new IndexOutOfBoundsException();
    } else if (size - 1 < index) {
      throw new IndexOutOfBoundsException();
    } else {
      while (true) {
        if (counter == index) {
          break;
        }
        counter++;
        previous = current;
        current = current.next;
      }
      if (counter == 0) {
       removeNode=head;
       insertNode.next=head.next;
       head=insertNode;
      } else {
        removeNode = previous.next;
        previous.next = insertNode;
        insertNode.next = current.next;

      }
      return removeNode.data;
    }
  }

  @Override
  public boolean isEmpty() {
    return size == 0;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public Object get(int index) {
    Node current = head;
    int counter = 0;
    if (head == null) {
      throw new IndexOutOfBoundsException();
    }
    while (counter != index && current.next != null) {
      counter++;
      current = current.next;
    }
    return current.data;
  }

  public Iterator iterator() {
    Iterator iterator = new Iterator() {

      Node current = head;

      @Override
      public boolean hasNext() {
        return current != null;
      }

      @Override
      public Object next() {
        if (size == 0 && !hasNext()) {
          throw new NoSuchElementException();
        }
        if (hasNext()) {
          Object data = current.data;
          current = current.next;
          return data;
        }

        return null;
      }
    };
    return iterator;
  }

  public static void main(String[] args) {

    MyLinkedList linkList = new MyLinkedList();

    linkList.add("abc");
    linkList.add("def");
    linkList.add("ghi");
    linkList.add("jkl");
    System.out.println("linkList.set(2,\"new\") = " + linkList.set(2, "new"));
//    System.out.println(linkList.set(1, "fuck"));
    // linkList.deleteNodeByValue("hello");
    // linkList.remove(1);
    //linkList.set(2, 69);
    // System.out.println(linkList.get(1));
    linkList.display();
  }
}
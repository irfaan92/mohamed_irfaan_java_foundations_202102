package com.psybergate.grade2021.core.collections.exercise1.com.psybergate.mentoring.lang.collections;

import java.util.Collection;

public interface Array extends Collection {
  void push(Object obj);

  Object pop();

  Object get(int position);
}

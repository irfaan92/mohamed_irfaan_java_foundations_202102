package com.psybergate.grade2021.core.collections.exercise1.com.psybergate.mentoring.lang.collections;

import java.util.Collection;
import java.util.Iterator;

public class ArrayStack implements Array {

  public static void main(String[] args) {
    ArrayStack arrayStack = new ArrayStack();
    arrayStack.add(1);
    arrayStack.add(2);
    arrayStack.add(1);
    arrayStack.add(5);
    System.out.println("stack.size() = " + arrayStack.size());
   // System.out.println("stack.pop() = " + stack.pop());
    System.out.println("stack.size = " + arrayStack.size());
    System.out.println("stack.contains(2) = " + arrayStack.contains(7));
    for (Object o : arrayStack) {
      System.out.println(o);
    }
  }

  private Object[] obj = new Object[100];

  private int size;


  public void push(Object o){
    add(o);
  }

  public Object pop(){
    Object last= obj[--size];
    obj[size]=null;
    return last;
  }

  public Object get(int index){
    return obj[size-index];
  }
  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    if (size == 0) {
      return true;
    }
    return false;
  }

  @Override
  public boolean contains(Object o) {
    for (Object o1 : obj) {
      if (o.equals(o1)) {
        return true;
      }

    }
    return false;
  }

  @Override
  public Iterator iterator() {
    Iterator iterator = new Iterator() {
      int index = size;
      @Override
      public boolean hasNext() {
        return index-->0;
      }

      @Override
      public Object next() {
        return obj[index];
      }

    };
return iterator;
  }

  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public boolean add(Object o) {
    obj[size++]=o;
    return true;
  }

  @Override
  public boolean remove(Object o) {
    return false;
  }

  @Override
  public boolean addAll(Collection c) {
    return false;
  }

  @Override
  public void clear() {

  }

  @Override
  public boolean retainAll(Collection c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection c) {
    return false;
  }

  @Override
  public boolean containsAll(Collection c) {
    return false;
  }

  @Override
  public Object[] toArray(Object[] a) {
    return new Object[0];
  }

}

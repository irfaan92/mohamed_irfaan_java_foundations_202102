package com.psybergate.grad2021.core.ex2;

import com.psybergate.grad2021.core.ex1.DomainClass;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@DomainClass
public class Student implements  Serializable{
  public int studentNumber;
  private String studentName;
  private String degree;
  private int overallMark;
  private int age;

  public int getStudentNumber() {
    return studentNumber;
  }

  public void setStudentNumber(int studentNumber) {
    this.studentNumber = studentNumber;
  }

  public String getStudentName() {
    return studentName;
  }

  public void setStudentName(String studentName) {
    this.studentName = studentName;
  }

  public String getDegree() {
    return degree;
  }

  public void setDegree(String degree) {
    this.degree = degree;
  }

  public int getOverallMark() {
    return overallMark;
  }

  public void setOverallMark(int overallMark) {
    this.overallMark = overallMark;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public boolean passOrFailed(){
    return overallMark >50;
  }

  public Student() {
    System.out.println("default constructor");
  }

  public Student(int studentNumber, String degree, int overallMark) {
    this.studentNumber = studentNumber;
    this.degree = degree;
    this.overallMark = overallMark;
    System.out.println("please work");
  }

  public Student(int studentNumber, String studentName, String degree, int overallMark, int age) {
    this.studentNumber = studentNumber;
    this.studentName = studentName;
    this.degree = degree;
    this.overallMark = overallMark;
    this.age = age;
  }

  public void adjustedMark(){
    overallMark+=5;
  }

  public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
    Class aClass1 = Class.forName("com.psybergate.grad2021.core.ex2.Student");
    Object studentInstance1 = aClass1.newInstance();
    Class aClass2 = Class.forName("com.psybergate.grad2021.core.ex2.Student");
    Class[] type = {int.class,String.class,int.class};
    Constructor constructor = aClass2.getConstructor(type);
    Object[] obj={1,"irfaan",50};
    Object studentInstance2 = constructor.newInstance(obj);
    Method adjustedMark = aClass1.getMethod("adjustedMark");
    adjustedMark.invoke(studentInstance2);
    Method getMark = aClass1.getMethod("getOverallMark");
    System.out.println(getMark.invoke(studentInstance2));
  }
}

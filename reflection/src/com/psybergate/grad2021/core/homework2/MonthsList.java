package com.psybergate.grad2021.core.homework2;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

public class MonthsList {
  private static final String listClass;

  static {
    Properties properties = new Properties();
    try {
      properties.load(MonthsList.class
          .getClassLoader()
          .getResourceAsStream("com/psybergate/grad2021/core/homework2/name.txt"));
    } catch (IOException e) {
      throw new RuntimeException(e.getMessage(), e);
    }


    listClass = properties.getProperty("listClass");
  }

  public static void main(String[] args) throws Exception {
    Class<List> listClass = (Class<List>) Class.forName(MonthsList.listClass);

    List<String> list = listClass.newInstance();
    list.add("January");
    list.add("February");
    list.add("March");
    list.add("April");
    list.add("May");
    list.add("June");
    list.add("July");
    list.add("August");
    list.add("September");
    list.add("December");
    printList(list);
  }

  private static void printList(List<String> list) {
    System.out.println(list.getClass().getName());
    int i = 0;
    for (Object o : list) {
      System.out.println(i++ + 1 + ": " + o);
    }
  }
}

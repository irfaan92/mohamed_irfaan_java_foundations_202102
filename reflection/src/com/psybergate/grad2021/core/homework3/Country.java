package com.psybergate.grad2021.core.homework3;

public class Country {
  public String name;
  private int population;

  public Country(String name, int population) {
    this.name = name;
    this.population = population;
  }

  public Country() {
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getPopulation() {
    return population;
  }

}

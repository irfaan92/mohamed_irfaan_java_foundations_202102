package com.psybergate.grad2021.core.homework3;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.*;
import java.lang.invoke.MethodType;
import java.lang.reflect.Method;

public class Main {
  public static void main(String[] args) throws Throwable {
    Class<Country> countryClass = Country.class;
    Lookup lookup = MethodHandles.publicLookup();
    MethodType noArgs = MethodType.methodType(void.class);
    MethodHandle constructor = lookup.findConstructor(Country.class, noArgs);
    Object invoke1 = constructor.invoke();
    //Country country = new Country();
    MethodType methodType = MethodType.methodType(void.class, String.class);
    MethodHandle setName = lookup.findVirtual(countryClass, "setName", methodType);
    setName.invoke(invoke1, "Greece");
    MethodType methodType2 = MethodType.methodType(String.class);
    MethodHandle getName = lookup.findVirtual(Country.class, "getName", methodType2);
    String invoke = (String) getName.invoke(invoke1);
    System.out.println(invoke);
    MethodHandle getter = lookup.findGetter(countryClass, "name", String.class);
    System.out.println(getter.invoke(invoke1));
  }
}

package com.psybergate.grad2021.core.ex1;

import java.io.Serializable;

@DomainClass
public class Student implements  Serializable{
  public int studentNumber;
  private String studentName;
  private String degree;
  private int overallMark;
  private int age;

  public int getStudentNumber() {
    return studentNumber;
  }

  public void setStudentNumber(int studentNumber) {
    this.studentNumber = studentNumber;
  }

  public String getStudentName() {
    return studentName;
  }

  public void setStudentName(String studentName) {
    this.studentName = studentName;
  }

  public String getDegree() {
    return degree;
  }

  public void setDegree(String degree) {
    this.degree = degree;
  }

  public int getOverallMark() {
    return overallMark;
  }

  public void setOverallMark(int overallMark) {
    this.overallMark = overallMark;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public boolean passOrFailed(){
    return overallMark >50;
  }

  public Student() {
    System.out.println("default constructor");
  }

  public Student(int studentNumber, String degree, int overallMark) {
    this.studentNumber = studentNumber;
    this.degree = degree;
    this.overallMark = overallMark;
  }

  public Student(int studentNumber, String studentName, String degree, int overallMark, int age) {
    this.studentNumber = studentNumber;
    this.studentName = studentName;
    this.degree = degree;
    this.overallMark = overallMark;
    this.age = age;
  }

  public void adjustedMark(){
    overallMark+=5;
  }

}

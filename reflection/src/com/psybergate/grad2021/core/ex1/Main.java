package com.psybergate.grad2021.core.ex1;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Main {
  public static void main(String[] args) {
    Student irfaan = new Student();
    System.out.println(irfaan.getClass().getSimpleName());
    System.out.println(irfaan.getClass().getPackage());
    Class c = irfaan.getClass();
    Annotation[] annotations = c.getAnnotations();
    for (Annotation annotation : annotations) {
      System.out.println((DomainClass)annotation);
    }
    Class[] interfaces = c.getInterfaces();
    for (Class anInterface : interfaces) {
      System.out.println(anInterface);
    }
    Field[] declaredFields = c.getDeclaredFields();
    for (Field declaredField : declaredFields) {
      System.out.println(declaredField);
    }
    Field[] fields = c.getFields();
    for (Field field : fields) {
      System.out.println(field);
    }
    Constructor[] constructors = c.getConstructors();
    for (Constructor constructor : constructors) {
      Class[] parameterTypes = constructor.getParameterTypes();
      for (Class parameterType : parameterTypes) {
        System.out.print(parameterType+ " ");
      }
      System.out.println();
    }
    Method[] declaredMethods = c.getDeclaredMethods();
    for (Method declaredMethod : declaredMethods) {
      System.out.println(declaredMethod);

    }
  }
}

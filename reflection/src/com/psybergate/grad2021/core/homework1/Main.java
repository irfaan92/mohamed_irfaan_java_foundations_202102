package com.psybergate.grad2021.core.homework1;

import com.psybergate.grad2021.core.ex1.Student;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class Main {
  public static void main(String[] args) {
    Class studentClass = Student.class;
    System.out.println("studentClass.getSuperclass().getSimpleName() = " + studentClass.getSuperclass().getSimpleName());
    System.out.println("studentClass.getModifiers() = " + Modifier.toString(studentClass.getModifiers()));
    Field[] declaredFields = studentClass.getDeclaredFields();
    for (Field declaredField : declaredFields) {
      System.out.println(declaredField);
    }
    Method[] declaredMethods = studentClass.getDeclaredMethods();
    for (Method declaredMethod : declaredMethods) {
      System.out.println(declaredMethod);
      System.out.println(declaredMethod.getReturnType());
    }
  }
}

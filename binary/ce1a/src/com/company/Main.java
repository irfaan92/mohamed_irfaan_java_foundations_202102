package com.company;

import java.io.*;

public class Main {

    public static void main(String[] args) {
        try {
            File f = new File("text01.txt");     //Creation of File Descriptor for input file
            FileReader fr = new FileReader(f);   //Creation of File Reader object
            BufferedReader br = new BufferedReader(fr);  //Creation of BufferedReader object
            int c = 0;
            while ((c = br.read()) != -1)         //Read char by Char
            {
                System.out.println(c);
                char character = (char) c;          //converting integer to char
                System.out.println(character);        //Display the Character
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package com.psybergate.grade2021.core.ooppart2.demo1a.constructor;

public abstract class Shape {
  private final int length1;

  private final int length2;

  public Shape(int length1, int length2) {
    this.length1 = length1;
    this.length2 = length2;
  }

  abstract public int calcArea();

  abstract public int calcPerimeter();
}

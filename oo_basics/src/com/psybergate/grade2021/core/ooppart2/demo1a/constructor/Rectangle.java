package com.psybergate.grade2021.core.ooppart2.demo1a.constructor;

public class Rectangle extends Shape {

  //    public Shape(int length1, int length2){
//        super();
//        System.out.println("im a Rectangle");
//    }
  public Rectangle(int length1, int length2) {
    super(length1, length2);
  }

  @Override
  public int calcArea() {
    return 0;
  }

  @Override
  public int calcPerimeter() {
    return 0;
  }
}

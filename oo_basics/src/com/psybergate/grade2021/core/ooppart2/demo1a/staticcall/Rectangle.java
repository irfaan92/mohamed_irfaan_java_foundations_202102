package com.psybergate.grade2021.core.ooppart2.demo1a.staticcall;

import java.lang.*;

public class Rectangle extends Shape {

  public static int noOfShapes;

  public Rectangle(int length1, int length2) {
    super(length1, length2);
  }

  @Override
  public int calcArea() {
    return 0;
  }

  @Override
  public int calcPerimeter() {
    return 0;
  }

  public static void main(String[] args) {
    Shape shape = new Rectangle(5, 4);
    Rectangle.addNumberOfShapes();
    System.out.println(Rectangle.noOfShapes);
    System.out.println(Shape.noOfShapes);
    String name = String.valueOf(shape.getClass());
    System.out.println(name);
//        Shape.addNumberOfShapes();
//        Rectangle.addNumberOfShapes();
//        System.out.println(Rectangle.noOfShapes);
//        System.out.println(Shape.noOfShapes);
  }

  public static int addNumberOfShapes() {

    return noOfShapes += 10;

  }
}

package com.psybergate.grade2021.core.ooppart2.hw5a3.orders;

import com.psybergate.grade2021.core.ooppart2.hw5a3.Customer;
import com.psybergate.grade2021.core.ooppart2.hw5a3.DiscountPolicies;

public class InternationalOrder extends Order {
  public static final double DUTY_TAX = 0.10;

  public InternationalOrder(Customer customer,String discountType) {
    super(customer,discountType);
  }

  public void calcDiscountedAmount() {
    setTotal(super.getTotal() * (1 - getDiscountRate()));
  }

  public void importTax() {
    setTotal(super.getTotal() * (1 + DUTY_TAX));
  }

  public double getTotal() {
    runThroughOrderItems();
    calcDiscountedAmount();
    calcTax();
    importTax();
    return super.getTotal();
  }
}

package com.psybergate.grade2021.core.ooppart2.hw5a3.orders;

import com.psybergate.grade2021.core.ooppart2.hw5a3.Customer;
import com.psybergate.grade2021.core.ooppart2.hw5a3.DiscountPolicies;

import java.time.LocalDate;
import java.time.Period;

public class LocalOrder extends Order {

  private double discountRate;

  private int numberOfYearsAsCustomer;

  public LocalOrder(Customer customer,String discountType) {
    super(customer,discountType);
  }

  public void calcDiscountedAmount() {
    setTotal(super.getTotal() * (1 - getDiscountRate()));
  }

  @Override
  public double getTotal() {
    runThroughOrderItems();
    calcDiscountedAmount();
    calcTax();
    return super.getTotal();
  }
}


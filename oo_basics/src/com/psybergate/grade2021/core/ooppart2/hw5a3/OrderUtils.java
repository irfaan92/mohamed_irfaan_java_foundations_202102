package com.psybergate.grade2021.core.ooppart2.hw5a3;

import com.psybergate.grade2021.core.ooppart2.hw5a3.orders.InternationalOrder;
import com.psybergate.grade2021.core.ooppart2.hw5a3.orders.LocalOrder;
import com.psybergate.grade2021.core.ooppart2.hw5a3.orders.Order;

import java.util.ArrayList;
import java.util.List;

public class OrderUtils {

  public static List<Customer> customers = new ArrayList<Customer>();

  public static void main(String[] args) {
    Customer customer = new Customer("irfaan", "lenasia", "1", "gmail",
        1992, 2, 18);
    Order order1 = InternationalOrder(customer,"Volume");
    Order order2 = InternationalOrder(customer,"Volume");
    Order order3 = InternationalOrder(customer,"Volume");
    Order order7 = setLocalOrder(customer,"Volume");
    addOrderItem(order1, 500_000);
    addOrderItem(order2, 10);
    addOrderItem(order3, 10);
    addOrderItem(order7, 10);
    customer.addOrder(order1);
    customer.addOrder(order2);
    customer.addOrder(order3);
    customer.addOrder(order7);
    addCustomer(customer);
    Order order4 = setLocalOrder(customer,"Loyalty");
    Order order5 = setLocalOrder(customer,"Loyalty");
    Order order6 = setLocalOrder(customer,"Loyalty");
    Order order8 = InternationalOrder(customer,"Loyalty");
    addOrderItem(order4, 10);
    addOrderItem(order5, 10);
    addOrderItem(order6, 10);
    addOrderItem(order8, 10);
    Customer customer2 = new Customer("irfaan", "lenasia", "1", "gmail",
        1992, 2, 18);
    customer2.addOrder(order4);
    customer2.addOrder(order5);
    customer2.addOrder(order6);
    customer2.addOrder(order8);
    addCustomer(customer2);
    displayTotalCost();
  }

  public static void addCustomer(Customer customer) {
    OrderUtils.customers.add(customer);
  }

  public static void addOrderItem(Order order1, int quantity) {
    order1.addOrderItem(new OrderItems(new Product(), quantity));
  }

  public static Order setLocalOrder(Customer customer,String discountType) {
    return new LocalOrder(customer,discountType);
  }

  public static Order InternationalOrder(Customer customer,String discountType) {
    return new InternationalOrder(customer,discountType);
  }

  public static void displayTotalCost() {
    for (Customer customer : customers) {
      System.out.println("customer.getCustomerId() = " + customer.getCustomerNumber());
      System.out.println("customer.getTotalBalanceOfCustomer() = " + customer.getTotalBalanceOfCustomer());
    }
  }
}

package com.psybergate.grade2021.core.ooppart2.hw5a3.orders;

import com.psybergate.grade2021.core.ooppart2.hw5a3.Customer;
import com.psybergate.grade2021.core.ooppart2.hw5a3.DiscountPolicies;
import com.psybergate.grade2021.core.ooppart2.hw5a3.OrderItems;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Order {

  public static final double TAX_RATE = 0.15;

  private static DiscountPolicies discountPolicies = new DiscountPolicies();

  private int orderID;

  private String discountType;

  private LocalDate orderDate;

  private Customer customer;

  private double total;

  private double discountRate;

  private final List<OrderItems> orderItem = new ArrayList<OrderItems>();

  public double getTotal() {
    return total;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setTotal(double total) {
    this.total = total;
  }

  public Order(Customer customer, String discountType) {
    setOrderID();
    this.customer = customer;
    this.discountType=discountType;
    determineDiscountPercent();
  }

  public Order() {

  }

  public void setOrderID() {
    Random rand = new Random();
    int maxNumber = 100_00_000_00;
    int randomNumber = rand.nextInt(maxNumber) + 1;
    this.orderID = randomNumber;
  }

  public void calcSubtotal(OrderItems orderItems) {
    total += orderItems.getPrice();
  }

  public void runThroughOrderItems() {
    for (OrderItems orderItems : orderItem) {
      calcSubtotal(orderItems);
    }
  }

  public void calcTax() {
    total += total * TAX_RATE;
  }

  public void addOrderItem(OrderItems item) {
    this.orderItem.add(item);
  }

  public double getDiscountRate() {
    return discountRate;
  }

  public void determineDiscountPercent(){
    if(discountType=="Loyalty"){
      int numberOfYearsAsCustomer = Period.between(getCustomer().getCustomerRegistrationDate(), LocalDate.now()).getYears();
      discountRate=discountPolicies.determineDiscountRateByDuration(numberOfYearsAsCustomer);
    }
    else if (discountType=="Volume"){
      discountRate=discountPolicies.determineDiscountRateByVolume(total);
    }
  }

}

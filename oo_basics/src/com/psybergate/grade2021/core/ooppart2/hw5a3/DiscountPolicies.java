package com.psybergate.grade2021.core.ooppart2.hw5a3;

public class DiscountPolicies {

  public static final int LOWER_DISCOUNT_LIMIT_YEAR = 2;

  public static final int UPPER_DISCOUNT_LIMIT_YEAR = 5;

  public static final int LOWER_DISCOUNT_LIMIT_AMOUNT = 500_000;

  public static final int UPPER_DISCOUNT_LIMIT_AMOUNT = 100_000_000;


  public double determineDiscountRateByVolume(double total) {
    if (total < DiscountPolicies.LOWER_DISCOUNT_LIMIT_AMOUNT) {
      return  0;
    } else if (total <= DiscountPolicies.UPPER_DISCOUNT_LIMIT_AMOUNT) {
      return  0.05;
    } else {
       return  0.10;
    }

  }

  public double determineDiscountRateByDuration(int numberOfYearsAsCustomer ) {
    if (numberOfYearsAsCustomer < DiscountPolicies.LOWER_DISCOUNT_LIMIT_YEAR) {
      return 0;
    } else if (numberOfYearsAsCustomer < DiscountPolicies.UPPER_DISCOUNT_LIMIT_YEAR) {
      return 0.075;
    } else {
      return  0.125;
    }
  }
}

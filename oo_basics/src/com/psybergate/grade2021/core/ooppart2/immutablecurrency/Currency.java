package com.psybergate.grade2021.core.ooppart2.immutablecurrency;

public class Currency {
  private String country;

  private String currencySymbol;

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getCurrencySymbol() {
    return currencySymbol;
  }

  public void setCurrencySymbol(String currencySymbol) {
    this.currencySymbol = currencySymbol;
  }

  public Currency(String country, String currencySymbol) {
    this.country = country;
    this.currencySymbol = currencySymbol;
  }

  @Override
  public String toString() {
    return "Currency{" +
        "country='" + country + '\'' +
        ", currencySymbol='" + currencySymbol + '\'' +
        '}';
  }
}

package com.psybergate.grade2021.core.ooppart2.immutablecurrency;

import java.math.BigDecimal;

public class Money {
  public static void main(String[] args) {
    Currency currency = new Currency("zar", "R");
    Money money = new Money(10, currency);
    currency.setCountry("Uk");
    System.out.println(money);

  }

  private final BigDecimal amount;

  private final Currency currency;

  public Money(double amount, Currency currency) {
    this.amount = new BigDecimal(amount);
    this.currency = new Currency(currency.getCountry(), currency.getCurrencySymbol());
  }

  public Money add(Money money) {
    return new Money(this.amount.add(money.amount).doubleValue(), this.currency);
  }

  @Override
  public String toString() {
    return "Money{" +
        "amount=" + amount +
        ", currency=" + currency +
        '}';
  }
}

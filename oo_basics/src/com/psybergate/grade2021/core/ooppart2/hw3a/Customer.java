package com.psybergate.grade2021.core.ooppart2.hw3a;

public class Customer {
  private final int customerNum;

  public Customer(int customerNum) {
    this.customerNum = customerNum;
  }

  public static void main(String[] args) {
    Customer customer = new Customer(1);
    Customer customer1 = new Customer(1);
    Customer customer2 = customer;
    System.out.println(customer2 == customer);
    System.out.println(customer == customer1);
    System.out.println(customer.equals(customer1));
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Customer other = (Customer) obj;
    return this.customerNum == other.customerNum;
  }
}

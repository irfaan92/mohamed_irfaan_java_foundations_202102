package com.psybergate.grade2021.core.ooppart2.ce3a;

import java.math.BigDecimal;

public final class Money {

  public static void main(String[] args) {
    Money money = new Money(5);
    Money money2 = new Money(5);
    System.out.println("money.addMoney(money2) = " + money.addMoney(money2));
  }

  private final BigDecimal value;

  public Money(BigDecimal amount) {
    this.value = amount;
  }

  public Money(int amount) {
    this.value = new BigDecimal(amount);
  }

  public Money addMoney(Money money) {
    return new Money(value.add(money.value));
  }

}

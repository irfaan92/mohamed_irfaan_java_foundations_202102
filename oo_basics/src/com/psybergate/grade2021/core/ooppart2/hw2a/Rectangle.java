package com.psybergate.grade2021.core.ooppart2.hw2a;

public class Rectangle {
  public static void main(String[] args) {

    Rectangle rectangle = new Rectangle(2, 1);
    Rectangle rectangle1 = new Rectangle(2, 1);
    System.out.println(rectangle == rectangle1);
    System.out.println(rectangle.equals(rectangle1));

  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Rectangle other = (Rectangle) obj;
    return this.length == other.length && this.width == other.width;
  }

  public static int MAX_lENGTH = 200;

  public static int MAX_WIDTH = 100;

  public static final int MAX_AREA = 15000;

  private final int length;

  private final int width;

  private static int noOfRectangles;

  public Rectangle(int length, int width) {
    checkParameters(length, width);
    this.length = length;
    this.width = width;
  }

  public void checkParameters(int length, int width) {
    if (length > MAX_lENGTH || width > MAX_WIDTH || area() > MAX_AREA || width > length) {
      throw new RuntimeException();
    }
  }

  public int area() {
    return length * width;
  }

  public int perimeter() {
    return 2 * length + 2 * width;
  }

  public static void increaseRectangles() {
    noOfRectangles++;
  }

  public static void displayAmountOfRectangles() {
    System.out.println("noOfRectangles = " + noOfRectangles);
  }

  public static void displayMaxPerimeters() {
    System.out.println("MAX_AREA = " + MAX_AREA);
    System.out.println("MAX_WIDTH = " + MAX_WIDTH);
    System.out.println("MAX_lENGTH = " + MAX_lENGTH);
  }
}

package com.psybergate.grade2021.core.ooppart2.ce1a;

public class Account {
  private int accountNum = 1;

  private final String name = "Irfaan";

  private final String surName = "Mohamed";

  public Account() {
    System.out.println("accountNum = " + accountNum);
    System.out.println("name = " + name);
    System.out.println("surName = " + surName);
  }

  public Account(int accountNum) {
    this.accountNum = accountNum;
  }
}

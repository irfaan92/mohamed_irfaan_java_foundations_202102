package com.psybergate.grade2021.core.ooppart2.ce1a;

public class CurrentAccount extends Account {
  private final String branch = "Lenasia";

  private final int balance = 100;

  public CurrentAccount() {
    System.out.println("balance = " + balance);
    System.out.println("branch = " + branch);
  }

  public static void main(String[] args) {
    CurrentAccount currentAccount = new CurrentAccount();
    //CurrentAccount currentAccount1 = new Account();
  }

}

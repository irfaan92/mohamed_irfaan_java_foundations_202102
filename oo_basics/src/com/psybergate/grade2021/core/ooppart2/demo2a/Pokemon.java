package com.psybergate.grade2021.core.ooppart2.demo2a;

import com.psybergate.grade2021.core.ooppart2.hw2a.Rectangle;

import java.util.Arrays;
import java.util.List;

public class Pokemon {

  public static void main(String[] args) {
    List<Pokemon> pokemons = Arrays.asList(
        new Pokemon("Pikachu", "Lightining", 10),
        new Pokemon("bulbasaur", "plant", 8),
        new Pokemon("Chirazard", "fire", 10),
        new Pokemon("Squirtle", "water", 7)
    );
    Pokemon findPokemon = new Pokemon("Pikachu", "Lightining", 10);
    System.out.println("pokemons.contains(findPokemon) = " + pokemons.contains(findPokemon));

  }

  private final String name;

  private final String type;

  private final int level;

  public Pokemon(String name, String type, int level) {
    this.name = name;
    this.type = type;
    this.level = level;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Pokemon other = (Pokemon) obj;
    return this.name == other.name;
  }
}

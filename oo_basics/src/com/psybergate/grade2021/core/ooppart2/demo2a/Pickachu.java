package com.psybergate.grade2021.core.ooppart2.demo2a;

public class Pickachu {

  public static void main(String[] args) {
    Pickachu pickachu = new Pickachu();
    Pickachu pickachu1 = pickachu;
    Pickachu pickachu2 = new Pickachu();
    System.out.println(pickachu == pickachu2);
  }

  public void speak() {
    System.out.println("pikaaaaaachu!!!!");
  }

}

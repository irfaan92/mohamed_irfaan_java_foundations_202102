package com.psybergate.grade2021.core.ooppart2.hw5a1;

public class OrderItems {
  private final Product product;

  private final int quantity;

  public OrderItems(Product product, int quantity) {
    this.product = product;
    this.quantity = quantity;
  }

  public int getQuantity() {
    return quantity;
  }

  public int getPrice() {
    return quantity * product.getPricePerUnit();
  }
}

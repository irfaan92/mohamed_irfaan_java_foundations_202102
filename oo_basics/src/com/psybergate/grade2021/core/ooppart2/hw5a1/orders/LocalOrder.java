package com.psybergate.grade2021.core.ooppart2.hw5a1.orders;

import com.psybergate.grade2021.core.ooppart2.hw5a1.Customer;

import java.time.LocalDate;

public class LocalOrder extends Order {

  public static final double DISCOUNT_RATE = 0.10;

  public LocalOrder(Customer customer, LocalDate orderDate) {
    super(customer);
  }

  public void calcDiscountedAmount() {
    setTotal(super.getTotal() * (1 - DISCOUNT_RATE));
  }

  @Override
  public double getTotal() {
    runThroughOrderItems();
    calcDiscountedAmount();
    calcTax();
    return super.getTotal();
  }
}


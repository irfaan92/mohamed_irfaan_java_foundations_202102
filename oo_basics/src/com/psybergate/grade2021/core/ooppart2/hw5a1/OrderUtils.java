package com.psybergate.grade2021.core.ooppart2.hw5a1;

import com.psybergate.grade2021.core.ooppart2.hw5a1.orders.InternationalOrder;
import com.psybergate.grade2021.core.ooppart2.hw5a1.orders.LocalOrder;
import com.psybergate.grade2021.core.ooppart2.hw5a1.orders.Order;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class OrderUtils {

  public static List<Customer> customers = new ArrayList<Customer>();

  public static void main(String[] args) {
    Customer customer = new Customer("irfaan", "lenasia", "1", "gmail");
    Order order1 = InternationalOrder(customer, LocalDate.now());
    Order order2 = InternationalOrder(customer, LocalDate.now());
    Order order3 = InternationalOrder(customer, LocalDate.now());
    Order order7 = setLocalOrder(customer, LocalDate.now());
    addOrderItem(order1, 10);
    addOrderItem(order2, 10);
    addOrderItem(order3, 10);
    addOrderItem(order7, 10);
    customer.addOrder(order1);
    customer.addOrder(order2);
    customer.addOrder(order3);
    customer.addOrder(order7);
    addCustomer(customer);
    Order order4 = setLocalOrder(customer, LocalDate.now());
    Order order5 = setLocalOrder(customer, LocalDate.now());
    Order order6 = setLocalOrder(customer, LocalDate.now());
    Order order8 = InternationalOrder(customer, LocalDate.now());
    addOrderItem(order4, 10);
    addOrderItem(order5, 10);
    addOrderItem(order6, 10);
    addOrderItem(order8, 10);
    Customer customer2 = new Customer("Kubaib", "lenasia", "1", "gmail");
    customer2.addOrder(order4);
    customer2.addOrder(order5);
    customer2.addOrder(order6);
    customer2.addOrder(order8);
    addCustomer(customer2);
    displayTotalCost();
  }

  public static void addCustomer(Customer customer) {
    OrderUtils.customers.add(customer);
  }

  public static void addOrderItem(Order order1, int quantity) {
    order1.addOrderItem(new OrderItems(new Product(), quantity));
  }

  public static Order setLocalOrder(Customer customer, LocalDate orderDate) {
    return new LocalOrder(customer, orderDate);
  }

  public static Order InternationalOrder(Customer customer, LocalDate orderDate) {
    return new InternationalOrder(customer, orderDate);
  }

  public static void displayTotalCost() {
    for (Customer customer : customers) {
      System.out.println("customer.getCustomerId() = " + customer.getCustomerNumber());
      System.out.println("customer.getTotalBalanceOfCustomer() = " + customer.getTotalBalanceOfCustomer());
    }
  }
}

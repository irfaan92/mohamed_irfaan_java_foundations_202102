package com.psybergate.grade2021.core.ooppart2.hw5a1;

import com.psybergate.grade2021.core.ooppart2.hw5a1.orders.Order;

import java.util.Random;
import java.util.ArrayList;
import java.util.List;

public class Customer {

  private int customerNumber;

  private final String customerName;

  private final String customerAddress;

  private final String customerPhoneNum;

  private final String customerEmail;

  private final List<Order> orders = new ArrayList<Order>();

  private Object customerOrderType;

  public Customer(String customerName, String customerAddress, String customerPhoneNum, String customerEmail) {
    setCustomerId();
    this.customerName = customerName;
    this.customerAddress = customerAddress;
    this.customerPhoneNum = customerPhoneNum;
    this.customerEmail = customerEmail;
  }

  public void setCustomerId() {
    Random rand = new Random();
    int maxNumber = 100_00_000_00;
    int randomNumber = rand.nextInt(maxNumber) + 1;
    customerNumber = randomNumber;
  }

  public int getCustomerNumber() {
    return customerNumber;
  }

  public void addOrder(Order order) {
    customerOrderType(order);
    if (validateOrder(order)) {
      orders.add(order);
    }
  }

  public double getTotalBalanceOfCustomer() {
    double totalBalanceOfCustomer = 0;
    for (Order order : orders) {
      totalBalanceOfCustomer += order.getTotal();
    }
    return totalBalanceOfCustomer;
  }

  private void customerOrderType(Order order) {
    if (orders.size() == 0) {
      customerOrderType = order;
    }
  }

  private boolean validateOrder(Order order) {
    return (customerOrderType.getClass() == order.getClass());
  }

}

package com.psybergate.grade2021.core.ooppart2.hw5a1.orders;

import com.psybergate.grade2021.core.ooppart2.hw5a1.Customer;

import java.time.LocalDate;

public class InternationalOrder extends Order {
  public static final double DUTY_TAX = 0.10;

  public InternationalOrder(Customer customer, LocalDate orderDate) {
    super(customer);
  }

  public void importTax() {
    setTotal(super.getTotal() * (1 + DUTY_TAX));
  }

  public double getTotal() {
    runThroughOrderItems();
    calcTax();
    importTax();
    return super.getTotal();
  }
}

package com.psybergate.grade2021.core.ooppart2.hw5a1.orders;

import com.psybergate.grade2021.core.ooppart2.hw5a1.Customer;
import com.psybergate.grade2021.core.ooppart2.hw5a1.OrderItems;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Order {

  public static final double TAX_RATE = 0.15;

  private int orderID;

  private LocalDate orderDate;

  private Customer customer;

  private double total;

  private final List<OrderItems> orderItem = new ArrayList<OrderItems>();

  public double getTotal() {
    return total;
  }

  public void setTotal(double total) {
    this.total = total;
  }

  public Order(Customer customer) {
    setOrderID();
    this.customer = customer;
  }

  public Order() {

  }

  public void setOrderID() {
    Random rand = new Random();
    int maxNumber = 100_00_000_00;
    int randomNumber = rand.nextInt(maxNumber) + 1;
    this.orderID = randomNumber;
  }

  public void calcSubtotal(OrderItems orderItems) {
    total += orderItems.getPrice();
  }

  public void runThroughOrderItems() {
    for (OrderItems orderItems : orderItem) {
      calcSubtotal(orderItems);
    }
  }

  public void calcTax() {
    total += total * TAX_RATE;
  }

  public void addOrderItem(OrderItems item) {
    this.orderItem.add(item);
  }

}

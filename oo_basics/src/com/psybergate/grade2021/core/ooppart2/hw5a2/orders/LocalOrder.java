package com.psybergate.grade2021.core.ooppart2.hw5a2.orders;

import com.psybergate.grade2021.core.ooppart2.hw5a2.Customer;

import java.time.LocalDate;
import java.time.Period;

public class LocalOrder extends Order {

  public static final int LOWER_DISCOUNT_LIMIT_YEAR = 2;

  public static final int UPPER_DISCOUNT_LIMIT_YEAR = 5;

  private double discountRate;

  private int numberOfYearsAsCustomer;

  public LocalOrder(Customer customer) {
    super(customer);
    determineNumberOfYearsAsCustomer();
    determineDiscountRate();
  }

  public void determineNumberOfYearsAsCustomer() {
    int years = Period.between(getCustomer().getCustomerRegistrationDate(), LocalDate.now()).getYears();
    numberOfYearsAsCustomer = years;
  }

  public void determineDiscountRate() {
    if (numberOfYearsAsCustomer < LOWER_DISCOUNT_LIMIT_YEAR) {
      discountRate = 0;
    } else if (numberOfYearsAsCustomer < UPPER_DISCOUNT_LIMIT_YEAR) {
      discountRate = 0.075;
    } else {
      discountRate = 0.125;
    }
  }

  public void calcDiscountedAmount() {
    setTotal(super.getTotal() * (1 - discountRate));
  }

  @Override
  public double getTotal() {
    runThroughOrderItems();
    calcDiscountedAmount();
    calcTax();
    return super.getTotal();
  }
}


package com.psybergate.grade2021.core.ooppart2.hw5a2.orders;

import com.psybergate.grade2021.core.ooppart2.hw5a2.Customer;

public class InternationalOrder extends Order {
  public static final double DUTY_TAX = 0.10;

  public static final int LOWER_DISCOUNT_LIMIT_AMOUNT = 500_000;

  public static final int UPPER_DISCOUNT_LIMIT_AMOUNT = 100_000_000;

  private double discountRate;

  public InternationalOrder(Customer customer) {
    super(customer);
  }

  public void determineDiscountRate() {
    if (super.getTotal() < LOWER_DISCOUNT_LIMIT_AMOUNT) {
      discountRate = 0;
    } else if (super.getTotal() <= UPPER_DISCOUNT_LIMIT_AMOUNT) {
      discountRate = 0.05;
    } else {
      discountRate = 0.10;
    }

  }

  public void calcDiscountedAmount() {
    setTotal(super.getTotal() * (1 - discountRate));
  }

  public void importTax() {
    setTotal(super.getTotal() * (1 + DUTY_TAX));
  }

  public double getTotal() {
    runThroughOrderItems();
    determineDiscountRate();
    calcDiscountedAmount();
    calcTax();
    importTax();
    return super.getTotal();
  }
}

package com.psybergate.grade2021.core.ooppart2.hw5a2;

public class Product {
  private final String name = "pens";

  private final int pricePerUnit = 1;

  public String getName() {
    return name;
  }

  public int getPricePerUnit() {
    return pricePerUnit;
  }
}

package com.psybergate.grade2021.core.ooppart2.ce2a;

public class PassByValue {
  private String displayMessage;

  public PassByValue(String displayMessage) {
    this.displayMessage = displayMessage;
  }

  public void setDisplayMessage(String displayMessage) {
    this.displayMessage = displayMessage;
  }

  public String getDisplayMessage() {
    return displayMessage;
  }

  public static void main(String[] args) {
    PassByValue passByValue = new PassByValue("bulbasaur");
    PassByValue passByValue1 = new PassByValue("Squirtale");
    swap(passByValue, passByValue1);
    System.out.println(passByValue.displayMessage);
    System.out.println(passByValue1.displayMessage);

  }

  public void printMessage() {
    System.out.println(displayMessage);
  }

  public static void swap(PassByValue pbr1, PassByValue pbr2) {
    pbr1.setDisplayMessage("pikachu");
    PassByValue pbr3 = pbr1;
    pbr1 = pbr2;
    pbr2 = pbr3;
    pbr1.printMessage();
    pbr2.printMessage();

  }
}

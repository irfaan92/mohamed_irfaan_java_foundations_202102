package com.psybergate.grade2021.core.ooppart1.Interface;


public class InterfaceTest {
    public static void main(){
        Animal animal = new Dog();
        animal.speak();
    }
}
interface Animal{
    void speak();

}

class Dog implements Animal{

    @Override
    public void speak() {
        System.out.println("woof woof");
    }
}
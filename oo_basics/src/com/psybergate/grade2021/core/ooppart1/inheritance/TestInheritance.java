package com.psybergate.grade2021.core.ooppart1.inheritance;

public class TestInheritance {
    public static void main(String[] args) {
        bird bird = new bird();
        bird.swear();
        penguin penguin = new penguin();
        penguin.hello();
        penguin.swear();
        ostrich ostrich = new ostrich();
        ostrich.sound();
        ostrich.hello();
        ostrich.swear();
    }
}

class bird {
    public void swear(){
        System.out.println("fuck");
    }
}

class penguin extends bird{
    public void hello(){
        System.out.println("bloody");
    }
}

class ostrich extends penguin{
    public void sound(){
        System.out.println("existence is pain");
    }
}





package com.psybergate.grade2021.core.ooppart1.ce4a1;

public class SavingsAccount extends Account {

    public static double INTEREST_RATE = 0.15;

    public SavingsAccount() {
    }

    public SavingsAccount(int accountNum, String name, String surName, int balance) {
        super(accountNum, name, surName, balance);
    }
    @Override
    public void interestCalculateAmount() {
        double totalBalance=INTEREST_RATE*getBalance()+getBalance();
        setBalance(totalBalance);
    }
}

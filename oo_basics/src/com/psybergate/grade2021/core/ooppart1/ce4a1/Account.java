package com.psybergate.grade2021.core.ooppart1.ce4a1;

public abstract class Account {
    private int accountNum;
    private String name;
    private String SurName;
    private double balance;
    private double interestRate;

    public double getBalance() {
        return balance;
    }

    public Account() {
    }

    public Account(int accountNum, String name, String surName, int balance) {
        this.accountNum = accountNum;
        this.name = name;
        SurName = surName;
        this.balance = balance;

    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void deposit(int amount) {
        balance += amount;
    }

    public void withdraw(int amount) {
        balance -= amount;
    }

    public abstract void interestCalculateAmount();
}

package com.psybergate.grade2021.core.ooppart1.ce4a1;

public class CurrentAccount extends Account {

    public static double INTEREST_RATE = 0.20;

    public CurrentAccount() {
    }

    public CurrentAccount(int accountNum, String name, String surName, int balance) {
        super(accountNum, name, surName, balance);
    }

    @Override
    public void interestCalculateAmount() {
        double totalBalance=INTEREST_RATE*getBalance()+getBalance();
        setBalance(totalBalance);
    }

    public void makeMoneySound(){
        System.out.println("Shake your money maker");
    }
}

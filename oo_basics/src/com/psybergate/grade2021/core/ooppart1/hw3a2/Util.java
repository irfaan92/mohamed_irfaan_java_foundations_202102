package com.psybergate.grade2021.core.ooppart1.hw3a2;

import com.psybergate.grade2021.core.ooppart1.hw3a1.Rectangle;

public class Util {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(4, 3);
        Rectangle rectangle1 = new Rectangle(2, 1);
        Rectangle rectangle2 = new Rectangle(6, 2);
        Rectangle.MAX_lENGTH = 250;
        Rectangle.MAX_WIDTH = 150;
    }
}

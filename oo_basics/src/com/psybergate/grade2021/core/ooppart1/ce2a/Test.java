package com.psybergate.grade2021.core.ooppart1.ce2a;

public class Test {
    public static void main(String[] args) {
        Account account = new CurrentAccount(1,"irfaan","mohamed",0,0.20,40000);
        System.out.println("account.needsToBeReviewed() = " + account.needsToBeReviewed());
        Account account1 = new CurrentAccount(2, "irfaan", "mohamed", 0, 0.20, 100);
        System.out.println("account.needsToBeReviewed() = " + account1.needsToBeReviewed());
        Account account2 = new CurrentAccount(3, "irfaan", "mohamed", 0, 0.20, 25000);
        System.out.println("account.needsToBeReviewed() = " + account2.needsToBeReviewed());
    }
}

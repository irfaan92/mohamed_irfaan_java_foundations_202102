package com.psybergate.grade2021.core.ooppart1.ce2a;

public class CurrentAccount extends Account {
    private double interestRate;
    private int overDraft;
    public static final int MAX_OVERDRAFT = 100_000;
    public static final double OVERDRAFT_PERCENT_LIMIT = 0.20;
    public static final double OVERDRAFT_AMOUNT_LIMIT = 50_000;


    public CurrentAccount(int accountNum, String name, String surName, int balance,
                          double interestRate, int overDraft) {
        super(accountNum, name, surName, balance);
        this.overDraft = overDraft;
        this.interestRate = interestRate;
    }

    public boolean isOverDrawn() {
        if (overDraft >= 0 && overDraft <= MAX_OVERDRAFT) {
            return true;
        } else {
            return false;
        }
    }

    public boolean needsToBeReviewed() {
        if (isOverDrawn() && (overDraft >= (OVERDRAFT_PERCENT_LIMIT * MAX_OVERDRAFT))){
            return true;
        }
        else if (isOverDrawn()&& (overDraft > OVERDRAFT_AMOUNT_LIMIT)){
            return true;
        }
        else
        return false;

    }

    @Override
    public void getAccountType() {
        System.out.println("Current Account");
    }

}

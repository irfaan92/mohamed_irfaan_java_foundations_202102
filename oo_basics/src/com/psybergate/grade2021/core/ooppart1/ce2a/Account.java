package com.psybergate.grade2021.core.ooppart1.ce2a;

abstract class Account {
    private int accountNum;
    private String name;
    private String SurName;
    private int balance;

    public Account(int accountNum, String name, String surName, int balance) {
        this.accountNum = accountNum;
        this.name = name;
        this.SurName = surName;
        this.balance = balance;
    }


    public int getBalance() {
        return balance;
    }

    public abstract boolean needsToBeReviewed();

    public abstract void getAccountType();
}


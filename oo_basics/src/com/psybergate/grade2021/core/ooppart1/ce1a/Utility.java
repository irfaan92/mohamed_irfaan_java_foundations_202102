package com.psybergate.grade2021.core.ooppart1.ce1a;


public class Utility {
    public static void main(String[] args) {
        Customer irfaan = new Customer(1, "1", "1");
        irfaan.addAccount(new Account(1, 1));
        irfaan.addAccount(new Account(2, 1));
        irfaan.addAccount(new Account(3, 1));
        Customer snoop = new Customer(2, "1", "1");
        snoop.addAccount(new Account(1, 2));
        snoop.addAccount(new Account(2, 2));
        snoop.addAccount(new Account(3, 2));
        Utility utility = new Utility();
        utility.totalBalance(irfaan);
        utility.totalBalance(snoop);

    }

    public void totalBalance(Customer customer) {
        int total = 0;
        for (Account account : customer.getAccounts()) {
            total += account.getBalance();
        }
        System.out.println(total);
    }
}

package com.psybergate.grade2021.core.ooppart1.ce1a;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private final int customerNum;
    private final String name;
    private String address;
    private List<Account> accounts = new ArrayList<Account>();

    public Customer(int customerNum, String name, String address) {
        this.customerNum = customerNum;
        this.name = name;
        this.address = address;
    }

    public void addAccount(Account account) {
        accounts.add(account);
    }

    public List<Account> getAccounts() {
        return accounts;
    }
}
package com.psybergate.grade2021.core.ooppart1.ce1a;

public class Account {
    private int accountNum;
    private int balance;


    public Account(int accountNum, int balance) {
        this.accountNum = accountNum;
        this.balance = balance;
    }

    public int getBalance() {

        return balance;

    }
}

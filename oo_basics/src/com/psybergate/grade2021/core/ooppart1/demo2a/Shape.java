package com.psybergate.grade2021.core.ooppart1.demo2a;

public abstract class Shape {
    public abstract String dimensions();
    public abstract String area();
    public void print(){
        System.out.println("The dimensions of shape is "+ this.dimensions()+" and the " +
                "coresspoding area is "+this.area());
    }
}

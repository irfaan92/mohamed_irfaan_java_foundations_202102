package com.psybergate.grade2021.core.ooppart1.demo2a;

public class Triangle extends Shape{
    int base;
    int height;

    public Triangle(int base, int height) {
        this.base = base;
        this.height = height;
    }

    @Override
    public String dimensions() {
         return Integer.toString(base)+" "+Integer.toString(height);
    }

    @Override
    public String area() {
        return Double.toString(0.5*base*height);
    }

}

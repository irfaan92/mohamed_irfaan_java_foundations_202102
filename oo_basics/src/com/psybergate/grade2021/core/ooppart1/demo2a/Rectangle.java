package com.psybergate.grade2021.core.ooppart1.demo2a;

public class Rectangle extends Shape{
    private int length1;
    private int length2;

    public Rectangle(int length1, int length2) {
        this.length1 = length1;
        this.length2 = length2;
    }


    @Override
    public String dimensions() {

        return Integer.toString(length1)+" " +Integer.toString(length2);
    }

    @Override
    public String area() {
        return Integer.toString(length1*length2);
    }
}

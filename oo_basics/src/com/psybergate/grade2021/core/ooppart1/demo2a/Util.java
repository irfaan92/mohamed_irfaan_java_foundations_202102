package com.psybergate.grade2021.core.ooppart1.demo2a;

public class Util {
    public static void main(String[] args) {
        Shape shape = new Circle(5);
        Shape shape1 = new Rectangle(4, 3);
        Shape shape2 = new Triangle(1, 2);
        shape1.print();
        shape.print();
        shape2.print();
    }
}

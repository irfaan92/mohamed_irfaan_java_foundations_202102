package com.psybergate.grade2021.core.ooppart1.demo2a;

public class Circle extends Shape{
    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public String dimensions() {
        return Integer.toString(radius);
    }

    @Override
    public String area() {
        return Double.toString(Math.PI*radius*radius);
    }
}

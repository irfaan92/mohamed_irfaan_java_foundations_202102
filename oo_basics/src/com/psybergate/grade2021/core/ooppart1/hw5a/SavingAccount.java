package com.psybergate.grade2021.core.ooppart1.hw5a;

public class SavingAccount extends Account{
    private int overdraft;

    public SavingAccount(int accountNum, int balance, int overdraft) {
        super(accountNum, balance);
        this.overdraft = overdraft;
    }
}

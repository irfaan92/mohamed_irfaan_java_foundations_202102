package com.psybergate.grade2021.core.ooppart1.hw5a;

import java.util.ArrayList;

public class Util {
    public static void main(String[] args) {
        Customer customer = new Customer("Irfaan", "Mohamed", 1, new ArrayList<Account>());
        customer.addAccount(new CurrentAccount(1,2,1));
        customer.addAccount(new CurrentAccount(2,2,1));
        customer.addAccount(new SavingAccount(3,2,1));
        customer.addAccount(new SavingAccount(4,2,1));
        CustomerUtils customerUtils = new CustomerUtils();
        customerUtils.printBalances(customer);

    }
}

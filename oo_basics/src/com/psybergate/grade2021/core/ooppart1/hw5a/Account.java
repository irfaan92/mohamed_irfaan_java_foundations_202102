package com.psybergate.grade2021.core.ooppart1.hw5a;

public class Account {
    private int accountNum;
    private int balance;

    public Account(int accountNum, int balance) {
        this.accountNum = accountNum;
        this.balance = balance;
    }

    public void deposit(int amount){
        balance+=amount;
    }

    public void withdraw(int amount){
        balance-=amount;
    }

    public int getAccountNum() {
        return accountNum;
    }

    public int getBalance() {
        return balance;
    }
}

package com.psybergate.grade2021.core.ooppart1.hw5a;

import java.util.List;

public class Customer {
    private String name;
    private String surname;
    private int customerNum;
    private List<Account> accounts;

    public Customer(String name, String surname, int customerNum, List<Account> accounts) {
        this.name = name;
        this.surname = surname;
        this.customerNum = customerNum;
        this.accounts = accounts;
    }

    public void addAccount(Account account) {
        accounts.add(account);
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public int getCustomerNum() {
        return customerNum;
    }
}

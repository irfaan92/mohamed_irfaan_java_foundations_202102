package com.psybergate.grade2021.core.ooppart1.hw5a;

public class CurrentAccount extends Account{
    private int minBalance;

    public CurrentAccount(int accountNum, int balance, int minBalance) {
        super(accountNum, balance);
        this.minBalance = minBalance;
    }
}

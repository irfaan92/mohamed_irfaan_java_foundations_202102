package com.psybergate.grade2021.core.ooppart1.hw5a;

public class CustomerUtils {
    public void printBalances(Customer customer){
        for (Account account : customer.getAccounts()) {
            System.out.println("customer.getCustomerNum()/account.getAccountNum()/account" +
                    ".getBalance() = " + customer.getCustomerNum() +"/" + account.getAccountNum() +
                   "/"  + account.getBalance());
        }


    }
}

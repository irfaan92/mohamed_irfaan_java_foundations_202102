package com.psybergate.grade2021.core.ooppart1.ce3b;

public class Dog extends Animal{
    private String name;
    private String breed;
    private int obedienceLevel;

    public Dog(int weight, int height, int age, String name, String breed) {
        super(weight, height, age);
        this.name = name;
        this.breed = breed;
    }

    public Dog(int weight, int height, int age, String name, String breed, int obedienceLevel) {
       this(weight, height, age, name, breed);
        this.obedienceLevel = obedienceLevel;
    }

    public void obedienceIncrease(){
        this.obedienceLevel++;
    }
    public void makeSound(){
        super.makeSound();
        System.out.println("Bark Bark");
    }


}

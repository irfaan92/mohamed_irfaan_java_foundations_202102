package com.psybergate.grade2021.core.ooppart1.ce3b;

public class Animal {
    private int weight;
    private int height;
    private int age;

    public Animal(int weight, int height, int age) {
        this.weight = weight;
        this.height = height;
        this.age = age;
    }

    public void makeSound(){
        System.out.println("Im a animal hear me Roar");
    }

    @Override
    public String toString() {
        return "Animal{" +
                "weight=" + weight +
                ", height=" + height +
                ", age=" + age +
                '}';
    }
}

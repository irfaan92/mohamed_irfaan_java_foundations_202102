package com.psybergate.grade2021.core.ooppart1.hw2a;

public class MyDateOO {
    private int day;
    private int month;
    private int year;

    public MyDateOO(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public void validateDay() {

    }

    public void compareDate(MyDateOO myDateOO) {

    }

    public void increaseDate(int days){

    }
}

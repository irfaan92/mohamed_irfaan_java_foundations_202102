package com.psybergate.grade2021.core.ooppart1.ce4a2;

public class SavingsAccount extends Account {

    public static final int ACCOUNT_NEEDS_REVIEWED_AMOUNT = 2000;
    public static final int OVERDRAWN_AMOUNT = 5000;

    public SavingsAccount(int accountNum, String name, String surName, int balance,
                          double interestRate) {
        super(accountNum, name, surName, balance, interestRate);
    }


    @Override
    public boolean needsToBeReviewed() {
        if (getBalance() < ACCOUNT_NEEDS_REVIEWED_AMOUNT) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isOverDrawn() {
        if (getBalance() < OVERDRAWN_AMOUNT) {
            return true;
        }
        return false;
    }

    @Override
    public void getAccountType() {
        System.out.println("This is a Savings Account");
    }
}

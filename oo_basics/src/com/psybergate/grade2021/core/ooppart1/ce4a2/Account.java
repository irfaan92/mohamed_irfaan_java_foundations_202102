package com.psybergate.grade2021.core.ooppart1.ce4a2;

abstract class Account {
    private int accountNum;
    private String name;
    private String SurName;
    private int balance;
    private int overDraft;
    private double interestRate;

    public Account(int accountNum, String name, String surName, int balance, int overDraft,
                   double interestRate) {
        this(accountNum, name, surName, balance, interestRate);
        this.overDraft = overDraft;
    }

    public Account(int accountNum, String name, String surName, int balance, double interestRate) {
        this.accountNum = accountNum;
        this.name = name;
        this.SurName = surName;
        this.balance = balance;
        this.interestRate = interestRate;
    }

    public int getOverDraft() {
        return overDraft;
    }

    public int getBalance() {
        return balance;
    }

    public abstract boolean needsToBeReviewed();

    public abstract boolean isOverDrawn();

    public abstract void getAccountType();
}

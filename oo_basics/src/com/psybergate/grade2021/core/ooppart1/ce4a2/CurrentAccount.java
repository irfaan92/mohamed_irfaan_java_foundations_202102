package com.psybergate.grade2021.core.ooppart1.ce4a2;

public class CurrentAccount extends Account {
    private double interestRate;
    public static final int MAX_OVERDRAFT = 100_000;
    public static final double OVERDRAFT_PERCENT_LIMIT = 0.20;
    public static final double OVERDRAFT_AMOUNT_LIMIT = 50000;


    public CurrentAccount(int accountNum, String name, String surName, int balance,
                          double interestRate, int overDraft) {
        super(accountNum, name, surName, balance, overDraft, interestRate);
        this.interestRate = interestRate;
    }

    public boolean isOverDrawn() {
        return getOverDraft() >= 0 && getOverDraft() <= MAX_OVERDRAFT;
    }


    @Override
    public void getAccountType() {
        System.out.println("This is a Current Account");
    }

    public boolean needsToBeReviewed() {
       return (1+OVERDRAFT_PERCENT_LIMIT)*getOverDraft()<getBalance();

    }

}

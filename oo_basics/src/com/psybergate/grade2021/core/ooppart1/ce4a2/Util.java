package com.psybergate.grade2021.core.ooppart1.ce4a2;

public class Util {
    public static void main(String[] args) {
        Account account1 = new CurrentAccount(1, "irfaan", "mohamed", 0,
                0.20, 50000);
        Account account2 = new CurrentAccount(1, "irfaan", "mohamed", 0,
                0.20, 10000);
        Account account3 = new SavingsAccount(1, "irfaan", "mohamed", 0, 0.20);
        Account account4 = new SavingsAccount(1, "irfaan", "mohamed", 5000, 0.20);

        System.out.println(account1.needsToBeReviewed());
        System.out.println(account2.needsToBeReviewed());
        System.out.println(account3.needsToBeReviewed());
        System.out.println(account4.needsToBeReviewed());
    }
}

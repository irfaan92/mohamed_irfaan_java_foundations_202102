package com.psybergate.grade2021.core.ooppart1.hw3a1;


public class Rectangle {
    public static void main(String[] args) {
        try {
            Rectangle rectangle = new Rectangle(2, 1);
            System.out.println(rectangle.area());
        } catch (Exception e) {
            e.printStackTrace();
        }
        increaseRectangles();
        increaseRectangles();
        increaseRectangles();
        increaseRectangles();
        increaseRectangles();
        increaseRectangles();
        System.out.println(noOfRectangles);
    }

    public static int MAX_lENGTH = 200;
    public static int MAX_WIDTH = 100;
    public static final int MAX_AREA = 15000;
    private int length;
    private int width;
    private static int noOfRectangles;

    public Rectangle(int length, int width) {
        checkParameters(length, width);
        this.length = length;
        this.width = width;
    }

    public void checkParameters(int length, int width) {
        if (length > MAX_lENGTH || width > MAX_WIDTH || area() > MAX_AREA || width > length) {
            throw new RuntimeException();
        }
    }

    public int area() {
        return length * width;
    }

    public int perimeter() {
        return 2 * length + 2 * width;
    }

    public static void increaseRectangles() {
        noOfRectangles++;
    }

    public static void displayAmountOfRectangles() {
        System.out.println("noOfRectangles = " + noOfRectangles);
    }

    public static void displayMaxPerimeters() {
        System.out.println("MAX_AREA = " + MAX_AREA);
        System.out.println("MAX_WIDTH = " + MAX_WIDTH);
        System.out.println("MAX_lENGTH = " + MAX_lENGTH);
    }
}

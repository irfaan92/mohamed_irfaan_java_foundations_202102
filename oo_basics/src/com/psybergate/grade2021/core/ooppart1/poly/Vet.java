package com.psybergate.grade2021.core.ooppart1.poly;



public class Vet {
    public void injection(animal animal) {
        animal.makeSound();
    }

    public static void main(String[] args) {
        Vet vet = new Vet();
        animal animal =new bear();
        vet.injection(new bear());

    }
}

class animal {
    public void speak() {
        System.out.println("Im hungry");
    }

    public void makeSound() {
        System.out.println("im a animal");
    }
}

class bear extends animal {
    public void talk() {
        System.out.println("Im hungry too");
    }
    public void makeSound() {
        System.out.println("grrrrr");
    }
}

class lion extends animal {

    public void makeSound() {
        System.out.println("rawr");
    }
}

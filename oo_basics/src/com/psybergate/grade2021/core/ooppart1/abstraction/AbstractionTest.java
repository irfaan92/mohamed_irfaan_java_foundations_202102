package com.psybergate.grade2021.core.ooppart1.abstraction;


public class AbstractionTest {
    public static void main(String[] args) {
        Dog dog = new Poodle();
        dog.speech();
        dog.song();
    }

}
    abstract class Dog {
        abstract void speech();

        public void song() {
            System.out.println("who let the dogs out");
        }
    }

    class Poodle extends Dog {
        public void speech() {
            System.out.println("moo");
        }
    }


package com.psybergate.grade2021.core.ooppart1.ce3a;

abstract class Account {
    private int accountNum;
    private String name;
    private String SurName;
    private int balance;
    private int overDraft;

    public Account(int accountNum, String name, String surName, int balance, int overDraft) {
        this.accountNum = accountNum;
        this.name = name;
        this.SurName = surName;
        this.balance = balance;
        this.overDraft = overDraft;
    }

    public int getOverDraft() {
        return overDraft;
    }

    public int getBalance() {
        return balance;
    }

    public abstract boolean needsToBeReviewed();

    public void getAccountType(){

    }
}

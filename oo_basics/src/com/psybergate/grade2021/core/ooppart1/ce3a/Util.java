package com.psybergate.grade2021.core.ooppart1.ce3a;


import java.util.ArrayList;
import java.util.List;

public class Util {
    public static void main(String[] args) {
        Account account = new CurrentAccount(1,"irfaan","mohamed",0,0.20,50000);
        account.needsToBeReviewed();
        List<CurrentAccount> list=new ArrayList<CurrentAccount>();
        list.add(new CurrentAccount(1,"irfaan","mohamed",0,0.20,50000));
        list.add(new CurrentAccount(2,"irfaan","mohamed",0,0.20,50000));
        list.add(new CurrentAccount(3,"irfaan","mohamed",0,0.20,50000));
        list.add(new CurrentAccount(4,"irfaan","mohamed",0,0.20,50000));
        list.get(0).setInterestRate(0.10);
        System.out.println(list.get(0).getInterestRate());
    }
}

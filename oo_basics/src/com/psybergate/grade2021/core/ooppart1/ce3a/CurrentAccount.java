package com.psybergate.grade2021.core.ooppart1.ce3a;



public class CurrentAccount extends Account {
    private double interestRate;
    public int overDraft;
    public static final int MAX_OVERDRAFT = 100_000;
    public static final double OVERDRAFT_PERCENT_LIMIT = 0.20;
    public static final double OVERDRAFT_AMOUNT_LIMIT = 50000;

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public CurrentAccount(int accountNum, String name, String surName, int balance, double interestRate, int overDraft) {
        super(accountNum, name, surName, balance,overDraft);
        this.interestRate = interestRate;
    }

    public boolean isOverDrawn() {
        if (getOverDraft() >= 0 && getOverDraft() <= MAX_OVERDRAFT) {
            return true;
        } else {
            return false;
        }
    }

    public boolean needsToBeReviewed() {
        if (isOverDrawn() && (getOverDraft() > OVERDRAFT_PERCENT_LIMIT * MAX_OVERDRAFT) || (getOverDraft() > OVERDRAFT_AMOUNT_LIMIT)) {
            return true;
        }
        return false;

    }

}

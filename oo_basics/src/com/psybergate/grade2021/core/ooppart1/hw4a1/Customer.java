package com.psybergate.grade2021.core.ooppart1.hw4a1;

public class Customer {
    private String name;
    private String surName;
    private String address;
    private int age;
    private int customerNum;
    private static int noOfCustomers;


    public Customer(String name, String surName, String address, int customerNum) {
        this.name = name;
        this.surName = surName;
        this.address = address;
        this.customerNum = customerNum;
    }

    public Customer(String name, String surName, String address, int age,
                    int customerNum) {
        this(name, surName, address, customerNum);
        this.age = age;

    }


    public void customerAgeCheck(int age) {
        if (age < 18) {
            throw new RuntimeException();
        }
    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                ", address='" + address + '\'' +
                ", customerNum=" + customerNum +
                '}';
    }

    public static void increaseCustomer() {
        noOfCustomers++;
    }
}

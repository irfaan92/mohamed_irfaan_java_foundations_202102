package com.psybergate.hw5a;

public class Human {
    private int age;
    private int height;
    private int weight;

    public Human(){

    }
    public Human(int age, int height, int weight) {
        this.age = age;
        this.height = height;
        this.weight = weight;
    }

    protected int birthday(){
        return age++;
    }
    public int increaseWeight(){
        return weight+=20;
    }
    public int increaseHeight(){
        return height++;
    }


}

package com.psybergate.Hw6a;

public class StaticModifier {
    static int b = 310;

    static public void add(){
        b++;
    }

    public static void main(String[] args) {
        System.out.println(b);
        add();
        System.out.println(b);
    }
}

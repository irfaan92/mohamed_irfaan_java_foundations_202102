package com.psybergate.hw4a;

import com.psybergate.hw5a.Human;

public class Operators extends Human{


    public static void main(String[] args) {
        int number =0;
        boolean operating = true;

        System.out.println(4%3);
        System.out.println(number++);
        System.out.println(++number);
        System.out.println(4==3);
        System.out.println(4==4);
        System.out.println(true||false);
        System.out.println(true&&false);
        System.out.println(4&3);
        System.out.println(4|3);
        System.out.println(number+=2);
        System.out.println(operating?2:3);

        Human irfaan = new Human(1,2,3);
        //System.out.println(irfaan.birthday());
    }
}

package com.psybergate.grade2021.core.ooppart1.homework3;

import java.util.Arrays;

public class Main {
  public static void main(String[] args) {
    String expr = "2*x^3 - 4/5*y + z^2";
    String delims = "[+\\-*/\\^ ]+"; // so the delimiters are:  + - * / ^ space
    String[] tokens = expr.split(delims);
    System.out.println(Arrays.toString(tokens));
  }
}

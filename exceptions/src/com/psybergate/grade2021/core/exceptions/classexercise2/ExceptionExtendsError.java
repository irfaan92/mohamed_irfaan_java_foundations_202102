package com.psybergate.grade2021.core.exceptions.classexercise2;

public class ExceptionExtendsError extends Error{
  public ExceptionExtendsError(String message) {
    super(message);
  }
}

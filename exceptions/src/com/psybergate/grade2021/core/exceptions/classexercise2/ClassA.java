package com.psybergate.grade2021.core.exceptions.classexercise2;

public class ClassA {
  public ClassA() {
  }

  public static void main(String[] args) {
    callMethod1();
  }
  public static void callMethod1(){
    ClassB.method1();
  }

  public static void callMethod2(){
    ClassB.method2();
  }

  public static void callMethod3(){
    ClassB.method3();
  }

  public static void callMethod4(){
    ClassB.method4();
  }
}

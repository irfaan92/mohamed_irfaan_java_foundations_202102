package com.psybergate.grade2021.core.exceptions.classexercise2;

public class ClassB {

  public static void method1() {
    Rte.rte();
  }

  public static void method2() {
    try {
      CheckedException.checkException();
    } catch (Throwable throwable) {
      throwable.printStackTrace();
    }

  }

  public static void method3() {
    try {
      throw new ExceptionExtendsError("hello");
    } catch (Error e) {
      e.printStackTrace();
    }
  }

  public static void method4(){
    try {
      throw new ExceptionExtendsThrowable();
    } catch (ExceptionExtendsThrowable exceptionExtendsThrowable) {
      exceptionExtendsThrowable.printStackTrace();
    }
  }
}

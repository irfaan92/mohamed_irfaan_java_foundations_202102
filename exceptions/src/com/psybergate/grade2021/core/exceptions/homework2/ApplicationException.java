package com.psybergate.grade2021.core.exceptions.homework2;

public class ApplicationException extends Throwable{
  public ApplicationException(String message, Throwable cause) {
    super(message, cause);
  }
}

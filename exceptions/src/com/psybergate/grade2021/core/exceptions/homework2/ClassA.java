package com.psybergate.grade2021.core.exceptions.homework2;

import java.sql.SQLException;

public class ClassA {

  public static void methodA() throws ApplicationException {
    methodB();

  }

  public static  void methodB() throws ApplicationException
  {
    try {
      throw new SQLException();
    } catch (SQLException throwables) {
     // throwables.printStackTrace();
      throw new ApplicationException("im working",throwables);
    }
  }

}

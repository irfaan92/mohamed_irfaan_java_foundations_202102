package com.psybergate.grade2021.core.exceptions.homework4;

import java.util.Scanner;

public class Controller {

  //private static AccountDB accountDB;

  private String value;
  AccountDB accountDB =new AccountDB();

  public void readInString() {
    Scanner myObj = new Scanner(System.in);  // Create a Scanner object
    value = myObj.nextLine();  // Read user input
  }

  public String getValue() {
    return value;
  }

  public static boolean inputName(Controller controller, AccountDB accountDB) {
    try {
      System.out.println("Kindly enter Username details");
      controller.readInString();
      String name = controller.getValue();
      if (!name.equals(accountDB.getAccountHolderName())) {
        AccountValidator.incorrectNameException();
      } else {
        return true;
      }
    } catch (Exception e) {
      System.out.println("Incorrect name entered please reenter");
    }
    return false;
  }

  public static boolean inputPassword(Controller controller, AccountDB accountDB) {
    try {
      System.out.println("Kindly enter password details");
      controller.readInString();
      String password = controller.getValue();
      if (!password.equals(accountDB.getAccountHolderPassword())) {
        AccountValidator.incorrectNameException();
      } else {
        return true;
      }
    } catch (Exception e) {
      System.out.println("Incorrect name entered please reenter");
    }
    return false;
  }

  public void addAmount(String amount){
    int depositAmount = Integer.parseInt(amount);
    int accountBalance = Integer.parseInt(accountDB.getAccountHolderBalance());
    accountBalance += depositAmount;
    accountDB.setBalance(String.valueOf(accountBalance));
    System.out.println(accountDB.getAccountHolderBalance());
  }

  public void withdrawAmount(String amount){
    try {
      AccountValidator.withdrawLimitException(amount);
      int withdrawAmount = Integer.parseInt(amount);
      int accountBalance = Integer.parseInt(accountDB.getAccountHolderBalance());
      accountBalance -= withdrawAmount;
      AccountValidator.negativeBalanceException(String.valueOf(accountBalance));
      accountDB.setBalance(String.valueOf(accountBalance));
      System.out.println(accountDB.getAccountHolderBalance());
    } catch (Throwable e) {
      System.out.println("Kindly review withdraw amount");
    }
  }
}

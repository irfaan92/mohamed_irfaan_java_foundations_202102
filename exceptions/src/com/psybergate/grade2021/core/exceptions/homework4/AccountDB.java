package com.psybergate.grade2021.core.exceptions.homework4;

import java.util.HashMap;
import java.util.Map;

public class AccountDB {
  private static Map<String ,String> accountInformation =new HashMap<String,String>();
  public AccountDB(){
    accountInformation.put("AccountHolderName","Irfaan");
    accountInformation.put("AccountHolderPassword","1234");
    accountInformation.put("Balance","100");
  }
  public String getAccountHolderName(){
   return accountInformation.get("AccountHolderName");
  }
  public String getAccountHolderPassword(){
    return accountInformation.get("AccountHolderPassword");
  }
  public String getAccountHolderBalance(){
    return accountInformation.get("Balance");
  }

  public void setBalance(String amount){
    accountInformation.put("Balance",amount);
  }
}

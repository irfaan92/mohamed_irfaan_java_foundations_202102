package com.psybergate.grade2021.core.exceptions.homework4;

public class ClientRunner {
  public static void main(String[] args) {
    Controller controller = new Controller();
    AccountDB accountDB = new AccountDB();
    String inputcommand = "";

    while (true) {
      if (controller.inputName(controller, accountDB)) {
        break;
      }
    }
    while (true) {
      if (controller.inputPassword(controller, accountDB)) {
        break;
      }
    }
    System.out.println("Press 1 to make a deposit, 2 to make a withdrawal");
    System.out.println("Press stop to determinate program");
    while (true) {
      try {
        inputcommand = getInputcommand(controller);
        if (inputcommand.equals("1")) {
          System.out.println("Enter amount to deposit");
          controller.readInString();
          controller.addAmount(controller.getValue());
        } else if (inputcommand.equals("2")) {
          System.out.println("Enter amount to withdraw");
          controller.readInString();
          controller.withdrawAmount(controller.getValue());

        } else if (inputcommand.equals("stop")) {
          System.out.println("Thank you for using the service. Please come again");
        } else {
          AccountValidator.invalidOption();
        }
      } catch (Exception e) {
        System.out.println("Invalid Option");
      }
    }

  }

  private static String getInputcommand(Controller controller) {
    String inputcommand;
    controller.readInString();
    inputcommand = controller.getValue();
    return inputcommand;
  }

}

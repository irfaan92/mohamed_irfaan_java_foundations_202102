package com.psybergate.grade2021.core.exceptions.homework4;

public class AccountValidator extends Throwable {
  public static void incorrectNameException() {
    invalidOption();
  }

  public static void incorrectPasswordException() {
    invalidOption();
  }

  public static void negativeBalanceException(String amount) {
    if(Integer.valueOf(amount)<0)
      invalidOption();
  }

  public static void withdrawLimitException(String amount) {
    if(Integer.valueOf(amount)>200)
      invalidOption();
  }

  public static void invalidOption() {
    throw new IllegalArgumentException();
  }

}

package com.psybergate.grade2021.core.exceptions.classexercise3;

public class ChainException {
  public static void main(String[] args) {
    try {
      method1();
    } catch (RuntimeException e) {
      try {
//     throw new Throwable().initCause(e);
        Throwable throwable= new Throwable().initCause(e);
        throwable.printStackTrace();
      } catch (Throwable throwable) {
        System.out.println(throwable.getCause());
        throwable.printStackTrace();
      }
    }
  }

  public static void method1(){
    throw new RuntimeException();
  }
}

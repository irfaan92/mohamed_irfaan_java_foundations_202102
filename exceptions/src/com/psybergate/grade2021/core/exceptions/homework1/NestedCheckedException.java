package com.psybergate.grade2021.core.exceptions.homework1;

public class NestedCheckedException {
  public static void main(String[] args) {
    try {
      method1();
    } catch (Throwable throwable) {
      throwable.printStackTrace();
    }
  }

  public static void method1() throws Throwable {
method2();
  }

  public static void method2() throws Throwable {
method3();
  }

  public static void method3() throws Throwable {
method4();
  }

  public static void method4() throws Throwable {
method5();
  }

  public static void method5() throws Throwable {
    throw new Throwable();
  }
}

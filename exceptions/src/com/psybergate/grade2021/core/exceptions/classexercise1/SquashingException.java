package com.psybergate.grade2021.core.exceptions.classexercise1;

public class SquashingException {
  public static void main(String[] args) {
   try {
      new SquashingException();
     System.out.println("yes");
   } catch (Exception e) {
    System.out.println(e.getMessage());
    // throw e;
   }
    System.out.println("hello");
  }
  public SquashingException() throws RuntimeException {
    throw new RuntimeException("hello my friend");
  }
}

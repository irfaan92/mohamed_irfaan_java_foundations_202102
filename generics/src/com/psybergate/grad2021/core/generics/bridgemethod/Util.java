package com.psybergate.grad2021.core.generics.bridgemethod;

public class Util {
  public static void main(String[] args) {
    MyNode mn = new MyNode(5);
    Node n = mn;            // A raw type - compiler throws an unchecked warning
    n.setData("Hello");
    Integer x = mn.data;    // Causes a ClassCastException to be thrown.
  }
}

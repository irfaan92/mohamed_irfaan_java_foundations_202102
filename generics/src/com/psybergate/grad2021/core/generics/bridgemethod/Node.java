package com.psybergate.grad2021.core.generics.bridgemethod;

public class Node<T> {

  public T data;

  public Node(T data) { this.data = data; }

  public void setData(T data) {
    System.out.println("Node.setData");
    this.data = data;
  }
}
package com.psybergate.grad2021.core.generics.homework1c;

import java.util.ArrayList;
import java.util.Collections;

public class Homework3 {
  public ArrayList<String> reverse(ArrayList<String> input) {
    Collections.reverse(input);
    return input;
  }
}

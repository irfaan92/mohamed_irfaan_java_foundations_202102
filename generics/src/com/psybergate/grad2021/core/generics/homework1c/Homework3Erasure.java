package com.psybergate.grad2021.core.generics.homework1c;

import java.util.ArrayList;
import java.util.Collections;

public class Homework3Erasure {
  public ArrayList reverse(ArrayList input) {
    Collections.reverse(input);
    return input;
  }
}

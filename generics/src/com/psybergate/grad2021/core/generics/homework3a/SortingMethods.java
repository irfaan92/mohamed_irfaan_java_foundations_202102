package com.psybergate.grad2021.core.generics.homework3a;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SortingMethods {
  public static void main(String[] args) {
    List list = new ArrayList();
    list.add(1);
    list.add(2);
    list.add(3);
    list.add(4);
    System.out.println("evenNumbers(list) = " + evenNumbers(list));
  }
  public static List evenNumbers(Collection<Integer> integers){
    List evenNumberCollection = new ArrayList();
    for (Integer integer : integers) {
      if (integer%2==0) {
        evenNumberCollection.add(integer);
      }
    }
    return evenNumberCollection;
  }
  public static List oddNumbers(Collection<Integer> integers){
    List oddNumberCollection = new ArrayList();
    for (Integer integer : integers) {
      if (integer%2!=0) {
        oddNumberCollection.add(integer);
      }
    }
    return oddNumberCollection;
  }

  public static List primeNumbers(Collection<Integer> integers){
    List primeNumberCollection = new ArrayList();
    for (Integer integer : integers) {
      if(integer==0||integer==1){
        continue;
      }
      for (int i = 2; i < integer; i++) {
        if (integer % i == 0) {
          break;
        }
      }
      primeNumberCollection.add(integer);
    }
    return primeNumberCollection;
  }

  public static List cLetters(Collection<String> strings){
    List cLetterCollection = new ArrayList();
    for (String string : strings) {
      if ("c".equals(string.charAt(0))) {
        cLetterCollection.add(string);
      }
    }
    return cLetterCollection;
  }

  public static int noOfDraftedAccount(Collection<Account> accounts) {
    int noOfAccountsNegativeBalance = 0;
    for (Account account : accounts) {
      if (account.getBalance()<0) {
        noOfAccountsNegativeBalance++;
      }
    }
    return noOfAccountsNegativeBalance;
  }
}
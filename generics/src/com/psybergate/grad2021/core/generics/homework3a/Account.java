package com.psybergate.grad2021.core.generics.homework3a;

public class Account {
  private int balance;

  public Account(int balance) {
    this.balance = balance;
  }

  public int getBalance() {
    return balance;
  }
}

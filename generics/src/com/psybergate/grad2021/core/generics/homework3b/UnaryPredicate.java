package com.psybergate.grad2021.core.generics.homework3b;

public interface UnaryPredicate<T> {
  public boolean test(T obj);
}

package com.psybergate.grad2021.core.generics.homework3b;

public class CLetterPredicate implements UnaryPredicate<String>{
  @Override
  public boolean test(String obj) {
    return "c".equals(obj.charAt(0));
  }
}

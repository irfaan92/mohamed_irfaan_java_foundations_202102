package com.psybergate.grad2021.core.generics.homework3b;

public class PrimePredicate implements UnaryPredicate<Integer> {
  @Override
  public boolean test(Integer obj) {
    if (obj==1 || obj==0) {
      return false;
    }
    for (int i = 2; i < obj; i++) {
      if (obj % i == 0) {
        return false;
      }
    }
    return true;
  }
}

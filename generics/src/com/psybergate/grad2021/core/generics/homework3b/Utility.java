package com.psybergate.grad2021.core.generics.homework3b;

import java.util.Arrays;
import java.util.Collection;

public class Utility {
  public static void main(String[] args) {
    Collection<Integer> c1 = Arrays.asList(1,2,3,4,5,6);
    System.out.println(c1.getClass());
    int count= Algorithm.countIf(c1,new OddPredicate());
    System.out.println(count);
  }
}

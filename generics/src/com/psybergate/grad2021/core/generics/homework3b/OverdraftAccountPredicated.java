package com.psybergate.grad2021.core.generics.homework3b;

import com.psybergate.grad2021.core.generics.homework3a.Account;

public class OverdraftAccountPredicated implements UnaryPredicate<Account>{
  @Override
  public boolean test(Account obj) {
    return obj.getBalance()<0;
  }
}

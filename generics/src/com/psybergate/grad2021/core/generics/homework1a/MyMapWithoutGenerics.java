package com.psybergate.grad2021.core.generics.homework1a;

public class MyMapWithoutGenerics {
  private Object key;

  private Object value;

  public MyMapWithoutGenerics(Object key,Object value) {
    this.key = key;
    this.value = value;
  }

  public Object getKey() {
    return key;
  }

  public void setKey(Object key) {
    this.key = key;
  }

  public Object getValue() {
    return value;
  }

  public void setValue(Object value) {
    this.value = value;
  }
}

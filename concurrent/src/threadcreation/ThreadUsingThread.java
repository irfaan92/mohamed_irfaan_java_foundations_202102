package threadcreation;

public class ThreadUsingThread extends Thread{
  @Override
  public void run() {
    System.out.println(Thread.currentThread().getId());
  }
}

class Multithread1{
  public static void main(String[] args) {
    int n=8;
    for (int i =0;i<n;i++) {
      ThreadUsingThread threadUsingThread = new ThreadUsingThread();
      threadUsingThread.start();
    }
  }

}

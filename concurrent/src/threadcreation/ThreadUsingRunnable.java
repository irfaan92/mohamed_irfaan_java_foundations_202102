package threadcreation;

public class ThreadUsingRunnable implements Runnable{
  private int number;
  public ThreadUsingRunnable(int number) {
    this.number=number;
  }

  @Override
  public void run() {
    System.out.println(number);
  }
}
class Multithread{
  public static void main(String[] args) {
    int n =8;
    for (int i =0;i<n;i++) {
      Thread thread = new Thread(new ThreadUsingRunnable(i));
      thread.start();
    }

  }
}
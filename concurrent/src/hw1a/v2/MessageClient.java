package hw1a.v2;

public class MessageClient {
  public static void main(String[] args) {
    Thread writer = new Thread(new MessageWriterRunnable());
    Thread reader = new Thread(new MessageReaderRunnable());
    writer.start();
    reader.start();
  }
}

package hw1a.v1;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class MessageReader {
  private MessageWriter messageWriter;
  private static int noOfReaders;


  public MessageReader(){

  }

  public void reader() throws InterruptedException {
    List<Integer> MessageList = messageWriter.messages;
    System.out.println("MessageReader waiting for Messages"+noOfReaders++);
    TimeUnit.SECONDS.sleep(2);

    synchronized (this){
      if (checkNoOfMessages()!=0) {
        for (Integer integer : MessageList) {
          System.out.println(integer);
        }
        TimeUnit.SECONDS.sleep(3);
      }
      messageWriter.messages.clear();
      notify();
    }
  }

  public int checkNoOfMessages(){
    return messageWriter.messages.size();
  }
}

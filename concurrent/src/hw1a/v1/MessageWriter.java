package hw1a.v1;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MessageWriter {
  private String message;

  public static List<Integer> messages = new ArrayList<>();

  private volatile int messageCounter;


  public void setMessage(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public void setMessages() throws InterruptedException {
    synchronized (this){
    for (int i = 0; i < 10; i++) {
      TimeUnit.SECONDS.sleep(1);
        for (int j = 0; j < 10; j++) {
          messages.add(messageCounter++);
        }
        System.out.println("done writing one set of methods");
        TimeUnit.SECONDS.sleep(1);
        this.wait();
      }
    }
  }

  public List<Integer> getMessages() {
    return messages;
  }
}


package hw1a.v1;

import java.util.concurrent.TimeUnit;

public class Client {

  public static void main(String[] args) throws InterruptedException {
    TimeUnit.SECONDS.sleep(1);
    MessageReaderRunnable messageReaderRunnable = new MessageReaderRunnable();
    Thread thread = new Thread(messageReaderRunnable);
    Thread thread1 = new Thread(messageReaderRunnable);
    thread.start();
    thread1.start();
    Thread thread2 = new Thread(new MessageWriterRunnable());
    thread2.start();
  }
}

package ex1a;

public class Test implements Runnable{

  @Override
  public void run() {
    try {
      method1();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void method1() {
    method2();
  }

  private void method2() {
    method3();
  }

  private void method3()  {

    throw new RuntimeException();
  }
}

class Main{
  public static void main(String[] args) {
    Test test = new Test();
    Thread thread = new Thread(test);
    thread.start();
    Thread thread1 = new Thread(test);
    thread1.start();

  }
}

package ex3a.ungeneralized;

public class SumNumbersWithThreading implements Runnable{
  private int total;
  public void adder() {
    for (int i = 1; i < 10; i++) {
      total+=i;
    }
  }
  public int getTotal() {
    return total;
  }

  public static void main(String[] args) throws InterruptedException {
    long start = System.nanoTime();
    SumNumbersWithThreading sumNumbersWithThreading = new SumNumbersWithThreading();
    Thread t1 = new Thread(sumNumbersWithThreading);
    t1.start();
    //t1.join();
    Thread t2 = new Thread(sumNumbersWithThreading);
    t2.start();
    //t2.join();
    long end = System.nanoTime();
    System.out.println(sumNumbersWithThreading.getTotal());
    System.out.println(end-start);
  }

  @Override
  public void run() {
    adder();
  }

}


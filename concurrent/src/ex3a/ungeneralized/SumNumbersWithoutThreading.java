package ex3a.ungeneralized;

public class SumNumbersWithoutThreading {
  public static void main(String[] args) {
    long start = System.nanoTime();
    int total=0;
    for (int i =0;i<20;i++) {
      total+=i;
    }
    long end = System.nanoTime();
    System.out.println(total);
    System.out.println(end-start);
  }
}

package ex3a.generalized;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class Main {
  public static void main(String[] args) throws InterruptedException {
    ExecutorService service = Executors.newFixedThreadPool(10);
    Factorial factorial = new Factorial();

    IntStream.range(0, 10000)
        .forEach(count -> {
          service.submit(factorial::calculateSum);
        });
    service.awaitTermination(100000, TimeUnit.MILLISECONDS);

    System.out.println(factorial.getSum());
  }
}

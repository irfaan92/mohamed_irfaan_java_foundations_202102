package ex3a.generalized;

public class Factorial {
  private float counter=1;
  private float sum=1;

  public void incrementCounter(){
    counter++;
  }

  public void setSum(float sum) {
    this.sum = sum;
  }

  public float getSum() {
    return sum;
  }

  public float getCounter() {
    return counter;
  }

  public synchronized void calculateSum(){
    incrementCounter();
    setSum(getSum()*getCounter());
  }
}

package ex3a.factorialgeneralized;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class Factorial {
  private float counter=1;
  private float sum=1;

  public void incrementCounter(){
    counter++;
  }

  public void setSum(float sum) {
    this.sum = sum;
  }

  public float getSum() {
    return sum;
  }

  public float getCounter() {
    return counter;
  }

  public synchronized void calculateSum(){
    incrementCounter();
    setSum(getSum()*getCounter());
  }

  public static void main(String[] args) throws InterruptedException {
    ExecutorService service = Executors.newFixedThreadPool(10);
    Factorial factorial = new Factorial();

    IntStream.range(1, 5)
        .forEach(count -> {
          service.submit(factorial::calculateSum);
        });
    service.awaitTermination(1000, TimeUnit.MILLISECONDS);

    System.out.println(factorial.getSum());
  }


}

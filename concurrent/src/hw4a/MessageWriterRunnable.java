package hw4a;


import static java.lang.Math.random;

public class MessageWriterRunnable implements Runnable{

  @Override
  public void run() {
    MessageWriter messageWriter = new MessageWriter();
    int counter=0;
    while (counter++<5){
      try {
        Thread.sleep((long) (random()*1000));
        String message = String.valueOf(random()*1000);
        System.out.println("message = " + message);
        messageWriter.write(message);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}

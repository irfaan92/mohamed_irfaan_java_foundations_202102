package hw4a;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MessageClient {
  public static void main(String[] args) {
    ScheduledThreadPoolExecutor threadPool
        = new ScheduledThreadPoolExecutor(3);
    Runnable writer = new MessageWriterRunnable();
    Runnable reader = new MessageReaderRunnable();
    threadPool.schedule(reader,1, TimeUnit.SECONDS);
    threadPool.schedule(writer,10, TimeUnit.SECONDS);
    threadPool.schedule(writer,2, TimeUnit.SECONDS);


  }
}

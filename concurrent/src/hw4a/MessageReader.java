package hw4a;

public class MessageReader {
  public void readMessages(){
    while (true) {
      String message = MessageCache.CACHE.read();
      if (message == null) {
        break;
      }
      System.out.println(message);
    }
  }
}

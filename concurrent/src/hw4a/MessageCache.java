package hw4a;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MessageCache {
  public static final MessageCache CACHE = new MessageCache();
  private List<String> messages = Collections.synchronizedList(new ArrayList<>());
  public void add(String message){
    messages.add(message);
  }
  public String read(){
    if(messages.size()==0){
      return null;
    }
    return messages.remove(0);
  }
}

package com.psybergate.grade2021.core.annotations.hw1a;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class DataBaseManager {
  private Connection c = null;

  private Statement stmt = null;

  private Map<String, String> dictionary = new HashMap<>();

  private String sql = "CREATE TABLE ";

  private static Class aClass = Customer.class;

  public DataBaseManager() {
    dictionary.put("String", "TEXT");
    dictionary.put("Integer", "INT");
  }

  public void setTableName() {
    Annotation annotation = aClass.getAnnotation(DomainClass.class);
    DomainClass domainClass = (DomainClass) annotation;
    sql += domainClass.name() + " (";
  }

  public void setColumnName() {
    Field[] fields = aClass.getDeclaredFields();
    for (Field field : fields) {
      if ((field.getAnnotation(DomainProperty.class) != null)) {
        sql += field.getName() + " ";
        sql += dictionary.get(field.getType().getSimpleName()) + ", ";
      }
    }
    sql = sql.substring(0, sql.length() - 2);
    sql += ")";
  }
  public void setupDatabaseTable(){
    try {
      Class.forName("org.postgresql.Driver");
      c = DriverManager
          .getConnection("jdbc:postgresql://localhost:5432/postgres",
              "postgres", "admin");
      System.out.println("Opened database successfully");
      stmt = c.createStatement();
      stmt.executeUpdate(sql);
      stmt.close();
      c.close();
    } catch (Exception e) {
      e.printStackTrace();
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
  }

  public void generateDatabase(){
    setTableName();
    setColumnName();
    setupDatabaseTable();
  }

}

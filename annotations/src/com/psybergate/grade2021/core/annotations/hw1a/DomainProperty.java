package com.psybergate.grade2021.core.annotations.hw1a;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DomainProperty {
  boolean nullable() default false;
  boolean unique()default false;
}

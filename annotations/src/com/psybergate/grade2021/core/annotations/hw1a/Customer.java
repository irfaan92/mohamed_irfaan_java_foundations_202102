package com.psybergate.grade2021.core.annotations.hw1a;

@DomainClass
public class Customer {
  @DomainProperty
  private String customerNum;

  @DomainProperty
  private String name;

  @DomainProperty
  private String surname;

  @DomainProperty
  private Integer dateOfBirth;

@DomainTransient
  private int age;

  public Customer(String customerNum, String name, String surname, Integer dateOfBirth) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public String getName() {
    return "'"+name+"'";
  }

  public String getSurname() {
    return "'"+surname+"'";
  }

  public Integer getDateOfBirth() {
    return dateOfBirth;
  }
}

package com.psybergate.grade2021.core.annotations.hw1a;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class JDBCExample {

    public static void main(String args[]) {
      Connection c = null;
      Statement stmt = null;
      Customer customer = new Customer("2", "Snoop", "Dog", 18021972);
      try {
        Class.forName("org.postgresql.Driver");
        c = DriverManager
            .getConnection("jdbc:postgresql://localhost:5432/postgres",
                "postgres", "admin");
        c.setAutoCommit(false);
        System.out.println("Opened database successfully");
        stmt =c.createStatement();
        String sql = "INSERT INTO Unnamed(customerNum,name,surname,dateOfBirth)"
            +"VALUES ("+customer.getCustomerNum()+", "+customer.getName()+", "+
            customer.getSurname()+", "+customer.getDateOfBirth()+");";
        stmt.executeUpdate(sql);
        stmt.close();
        c.commit();
        c.close();
      } catch (Exception e) {
        e.printStackTrace();
        System.err.println(e.getClass().getName()+": "+e.getMessage());
        System.exit(0);
      }
      System.out.println("Table created successfully");
    }
  }


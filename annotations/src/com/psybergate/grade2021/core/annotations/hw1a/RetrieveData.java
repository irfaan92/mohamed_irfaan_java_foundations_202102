package com.psybergate.grade2021.core.annotations.hw1a;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class RetrieveData {
  public static void main(String args[]) {
    Connection c = null;
    Statement stmt = null;
    try {
      Class.forName("org.postgresql.Driver");
      c = DriverManager
          .getConnection("jdbc:postgresql://localhost:5432/postgres",
              "postgres", "admin");
      c.setAutoCommit(false);
      System.out.println("Opened database successfully");

      stmt = c.createStatement();

      ResultSet rs = stmt.executeQuery("SELECT * FROM unnamed WHERE customernum = '2f';");
      while (rs.next()) {
        int customernum = rs.getInt("customernum");
        String name = rs.getString("name");
        String surname = rs.getString("surname");
        Integer dob = rs.getInt("dateofbirth");
        System.out.println("customernum = " + customernum);
        System.out.println("NAME = " + name);
        System.out.println("surname = " + surname);
        System.out.println("dob = " + dob);
        System.out.println();
      }
      rs.close();
      stmt.close();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Operation done successfully");
  }
}

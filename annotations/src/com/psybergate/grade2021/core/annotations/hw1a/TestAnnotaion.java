package com.psybergate.grade2021.core.annotations.hw1a;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class TestAnnotaion {
  private static Object StringUtils;

  public static void main(String[] args) {
    Connection c = null;
    Statement stmt = null;
    Map<String, String> dictionary = new HashMap<>();
    dictionary.put("String", "TEXT");
    dictionary.put("Integer", "INT");
    String sql = "CREATE TABLE ";
    Class aClass = Customer.class;
    Annotation annotation = aClass.getAnnotation(DomainClass.class);
    DomainClass domainClass = (DomainClass) annotation;
    sql += domainClass.name() + " (";
    Field[] fields = aClass.getDeclaredFields();
    for (Field field : fields) {
      if ((field.getAnnotation(DomainProperty.class) instanceof DomainProperty)) {
        sql += field.getName() + " ";
        sql += dictionary.get(field.getType().getSimpleName()) + ", ";
      }
    }
    sql = sql.substring(0, sql.length() - 2);
    sql += ")";
    System.out.println(sql);
    try {
      Class.forName("org.postgresql.Driver");
      c = DriverManager
          .getConnection("jdbc:postgresql://localhost:5432/postgres",
              "postgres", "admin");
      System.out.println("Opened database successfully");
      stmt = c.createStatement();
      stmt.executeUpdate(sql);
      stmt.close();
      c.close();
    } catch (Exception e) {
      e.printStackTrace();
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Table created successfully");
    Customer customer = new Customer("1", "Irfaan", "Mohamed", 18021992);

  }

}




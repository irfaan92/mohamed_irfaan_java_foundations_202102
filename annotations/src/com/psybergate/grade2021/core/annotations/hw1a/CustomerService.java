package com.psybergate.grade2021.core.annotations.hw1a;

import java.sql.*;

public class CustomerService {
  Connection c = null;

  Statement stmt = null;

  Customer customer;

  String sql = "INSERT INTO Unnamed(customerNum,name,surname,dateOfBirth)"
      + "VALUES (";

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public void addCustomerData() {
    sql += customer.getCustomerNum() + ", " + customer.getName() + ", " +
        customer.getSurname() + ", " + customer.getDateOfBirth() + ");";
  }

  public void writeCustomerToDataBase() {
    try {
      Class.forName("org.postgresql.Driver");
      c = DriverManager
          .getConnection("jdbc:postgresql://localhost:5432/postgres",
              "postgres", "admin");
      c.setAutoCommit(false);
      System.out.println("Opened database successfully");
      stmt = c.createStatement();
      stmt.executeUpdate(sql);
      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      e.printStackTrace();
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
  }

  public void saveCustomer() {
    addCustomerData();
    writeCustomerToDataBase();
  }

  public void getCustomer(String customerNum){
    try {
      Class.forName("org.postgresql.Driver");
      c = DriverManager
          .getConnection("jdbc:postgresql://localhost:5432/postgres",
              "postgres", "admin");
      c.setAutoCommit(false);
      System.out.println("Opened database successfully");

      stmt = c.createStatement();
      ResultSet rs = stmt.executeQuery("SELECT * FROM unnamed WHERE customernum = "+"'"+customerNum+"';");
      getCustomerInfo(rs);
      rs.close();
      stmt.close();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
  }

  private void getCustomerInfo(ResultSet rs) throws SQLException {
    while (rs.next()) {
      String customerNum = rs.getString("customernum");
      String name = rs.getString("name");
      String surname = rs.getString("surname");
      Integer dob = rs.getInt("dateofbirth");
     setCustomer(new Customer(customerNum,name,surname,dob));
    }
  }
}

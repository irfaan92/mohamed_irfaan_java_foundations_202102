package com.psybergate.grade2021.core.ooppart1.Homework1.withenum1;

public enum Movie {
  ACTION("Rush Hour"),
  HORROR("SAW"),
  COMEDY;

  private String movieName;
  Movie() {
  }
  Movie(String movieName){
    this.movieName=movieName;
  }
}
